package com.sales.crm.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Qualifier("authenticationService")
    @Autowired
    private UserDetailsService userDetailsService;

    public SecurityConfiguration() {
        super();
    }

    @Bean
    public AuthenticationProvider authProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(new BCryptPasswordEncoder());
        return provider;
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/sign-in").permitAll()
                .antMatchers("/sign-up").permitAll()
                .antMatchers("/user").authenticated()
                .antMatchers("/admin").authenticated()
                .antMatchers("/lead/*/send-email").authenticated()
                .antMatchers("/forgot-password").permitAll()
                .antMatchers("/reset-password").permitAll()
                .antMatchers("resend-verification-mail").permitAll()
                .antMatchers("/confirm-registration").permitAll()
                .antMatchers("/redirect").authenticated()
                .antMatchers("/send-message/**").permitAll()
                .antMatchers("/importUsers").hasRole("ADMIN")
                .antMatchers("/addUser").hasRole("ADMIN")
                .antMatchers("/addTask").hasAnyRole("USER","ADMIN")
                .antMatchers("/viewMyTasks").hasAnyRole("USER","ADMIN")
                .antMatchers("/viewOthersTasks").hasAnyRole("USER","ADMIN")
                .antMatchers("/editTask/**").hasAnyRole("USER","ADMIN")
                .antMatchers("/deleteTask/**").hasAnyRole("USER","ADMIN")
                .antMatchers("/getLeads").hasAnyRole("USER","ADMIN")
                .antMatchers("/changeTaskStatus").hasAnyRole("USER","ADMIN")
                .antMatchers("/view-emails").authenticated()
                .antMatchers("/send-email").authenticated()
                .antMatchers("/setting").authenticated()
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/lead/**").hasAnyRole("USER","ADMIN")
                .antMatchers("/lead-actions").hasAnyRole("USER","ADMIN")
                .antMatchers("/add-lead").hasRole("ADMIN")
                .antMatchers("/save-lead").hasAnyRole("USER","ADMIN")
                .antMatchers("/assign-deal").hasRole("ADMIN")
                .antMatchers("/all-deals").hasAnyRole("USER","ADMIN")
                .and()
                .formLogin()
                .loginPage("/sign-in")
                .permitAll()
                .and()
                .sessionManagement()
                .invalidSessionUrl("/invalidSession.html")
                .maximumSessions(1).sessionRegistry(sessionRegistry()).and()
                .sessionFixation().none()
                .and()
                .logout()
                .invalidateHttpSession(false)
                .logoutSuccessUrl("/")
                .deleteCookies("JSESSIONID")
                .permitAll()
                .and()
                .headers().frameOptions().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/webjars/**");
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }

}