package com.sales.crm.configuration;

import com.sales.crm.validation.EmailValidator;
import com.sales.crm.validation.PasswordMatchesValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import java.util.Locale;

@Configuration
@ComponentScan(basePackages = {"com.sales.crm.*"})
@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private MessageSource messageSource;

    public WebMvcConfig() {
        super();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Override
    public void configureDefaultServletHandling(final DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/static/");
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        final LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("lang");
        registry.addInterceptor(localeChangeInterceptor);
    }

    @Bean
    public LocaleResolver localeResolver() {
        final CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setDefaultLocale(Locale.ENGLISH);
        return cookieLocaleResolver;
    }

    @Bean
    public EmailValidator usernameValidator() {
        return new EmailValidator();
    }

    @Bean
    public PasswordMatchesValidator passwordMatchesValidator() {
        return new PasswordMatchesValidator();
    }


    @Bean
    @ConditionalOnMissingBean(RequestContextListener.class)
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }

    @Override
    public Validator getValidator() {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setValidationMessageSource(messageSource);
        return validator;
    }

    @Override
    public void addViewControllers(final ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index");
        registry.addViewController("add-lead.html");
        registry.addViewController("/admin-adminPage.html");
        registry.addViewController("/confirm-joining.html");
        registry.addViewController("/forgot-password.html");
        registry.addViewController("index.html");
        registry.addViewController("/invalidSession.html");
        registry.addViewController("/log-in.html");
        registry.addViewController("register-admin.html");
        registry.addViewController("/registration-successful");
        registry.addViewController("register-user.html");
        registry.addViewController("/send-email.html");
        registry.addViewController("/view-emails.html");
        registry.addViewController("/view-one-email.html");
        registry.addViewController("set-password.html");
        registry.addViewController("/user-adminPage.html");
        registry.addViewController("/sign-up.html");
        registry.addViewController("/successRegister.html");
        registry.addViewController("/reset-password.html");
        registry.addViewController("/update-post.html");
        registry.addViewController("/updatePassword.html");
        registry.addViewController("/sendMessage.html");
        registry.addViewController("/users.html");
        registry.addViewController("/addDeal.html");
        registry.addViewController("/leaddetails.html");
        registry.addViewController("/leadnavigation.html");


    }
}