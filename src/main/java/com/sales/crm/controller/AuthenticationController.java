package com.sales.crm.controller;

import com.sales.crm.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;


    @GetMapping("/sign-in")
    public String signIn(Model model) {
        return "loginPage";
    }

    @GetMapping("/redirect")
    public String redirection(Authentication authentication, Model model) {
        return authenticationService.redirection(authentication, model);
    }

    @GetMapping("/admin")
    public String adminPage(Authentication authentication, Model model, @RequestParam(value = "sortBy", defaultValue = "desc") String sortBy,
                            @RequestParam(value = "selectUser", defaultValue = "") String selectUser) {
        System.out.println(selectUser);
        return authenticationService.adminDashBoard(authentication, model, sortBy, selectUser);

    }

    @GetMapping("/user")
    public String userDashbord(Authentication authentication, Model model) {
        return authenticationService.userDashboard(authentication, model);

    }

    @GetMapping("/goToUserProfile/{userId}")
    public String goToUserProfile(@PathVariable("userId")Long userId,Model model) {
        return authenticationService.userProfile(userId,model);
    }

}
