package com.sales.crm.controller;

import com.sales.crm.dto.ResetPasswordDto;
import com.sales.crm.dto.UserDto;
import com.sales.crm.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
public class RegistrationController {
    @Autowired
    RegistrationService registrationService;

    @GetMapping("/")
    public String showLandingPage() {
        return "index";
    }

    @GetMapping("/sign-up")
    public ModelAndView showSignUpPage(Principal principal) {
        return registrationService.getSignUpPage("registerPage", principal);
    }

    @PostMapping("/sign-up")
    public ModelAndView registerNewManager(@ModelAttribute("userDto") @Valid UserDto userDto, BindingResult bindingResult,
                                           HttpServletRequest httpServletRequest, Principal principal) {
        return registrationService.registerNewUser(userDto, bindingResult, httpServletRequest, principal);
    }

    @PostMapping("/resend-verification-mail")
    public ModelAndView resendMail(final HttpServletRequest request,
                                   @RequestParam(name = "userEmail") String userEmail) {
        return registrationService.resendVerificationMail(request, userEmail);
    }

    @GetMapping("/confirm-registration")
    public String confirmRegistration(final HttpServletRequest request, final Model model,
                                      @RequestParam("token") final String token) {
        return registrationService.confirmRegister(request, model, token);
    }

    @GetMapping("/forgot-password")
    public ModelAndView showForgotPasswordPage() {
        return registrationService.showForgotPasswordPage();
    }

    @PostMapping("/forgot-password")
    public ModelAndView forgotPassword(@RequestParam("userEmail") final String userEmail) {
        return registrationService.sendForgotPasswordMail(userEmail);
    }

    @PostMapping("/reset-password")
    public ModelAndView resetPassword(@ModelAttribute("resetPasswordDto")
                                      @Valid final ResetPasswordDto resetPasswordDto,
                                      BindingResult bindingResult) {
        return registrationService.resetPassword(resetPasswordDto, bindingResult);
    }

    @GetMapping("/setting")
    public ModelAndView getUserSettingPage(Authentication authentication) {
        return registrationService.showUserSettingPage(authentication);
    }

    @PostMapping("/setting")
    public ModelAndView updateUserSetting(@ModelAttribute("user") @Valid final UserDto userDto,
                                          BindingResult bindingResult, Authentication authentication,
                                          final HttpServletRequest request) {
        return registrationService.updateUserSetting(request, bindingResult, authentication, userDto);
    }
}
