package com.sales.crm.controller;

import com.sales.crm.dto.IncorrectDataDto;
import com.sales.crm.dto.PasswordDto;
import com.sales.crm.dto.UserDto;
import com.sales.crm.model.User;
import com.sales.crm.repository.UserRepository;
import com.sales.crm.service.RegistrationService;
import com.sales.crm.service.UserService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.FileWriter;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;

@Controller
public class NewUserRegistrationController {

    @Autowired
    RegistrationService registrationService;
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;


    @GetMapping("/addUser")
    public ModelAndView addUser(Principal principal) {
        return registrationService.getSignUpPage("addUser", principal);
    }

    @GetMapping("/goBack")
    public String goBack(Principal principal) {
        User loggedInUser = userService.findUserByEmail(principal.getName());
        if(loggedInUser.getRole().getName().equals("ROLE_ADMIN")) {
            return "redirect:/admin";
        }
        else{
            return "redirect:/user";
        }
    }

    @PostMapping("/addUser")
    public ModelAndView registerNewUser(@ModelAttribute("userDto") @Valid UserDto userDto,
                                        BindingResult bindingResult,
                                        HttpServletRequest httpServletRequest, Principal principal) {
        return registrationService.registerNewUser(userDto, bindingResult, httpServletRequest, principal);
    }


    @PostMapping("/setPassword")
    public String setUserPassword(@ModelAttribute("passwordDto") @Valid PasswordDto dto,BindingResult errors, Model model) {
        if(errors.hasErrors()) {
            model.addAttribute("passwordDto", dto);
            return "setUserPassword";
        }
        if (!dto.getPassword().equals(dto.getMatchingPassword())) {
            model.addAttribute("passwordDto", dto);
            model.addAttribute("error", true);
            return "setUserPassword";
        }
        return registrationService.saveUser(dto);
    }

    @GetMapping("/importUsers")
    public String importUsers(Model model) {
        model.addAttribute("status", true);
        model.addAttribute("message", null);
        model.addAttribute("success", false);
        model.addAttribute("emailExists", false);
        model.addAttribute("incorrectData", false);
        return "importUsers";
    }

    @PostMapping("/upload-csv-file")
    public String uploadCSVFile(@RequestParam("file") MultipartFile file, Model model, Principal principal, HttpServletRequest httpServletRequest) {
        return registrationService.uploadFile(file, model, principal, httpServletRequest);
    }

    @PostMapping("/exportCsv")

    public void exportCsv(HttpServletResponse response,HttpServletRequest request)  {
        System.out.println("IN EXPORT CSV METHOD");

        registrationService.exportCsv(request,response);
    }

}
