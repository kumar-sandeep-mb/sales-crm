package com.sales.crm.controller;

import com.sales.crm.dto.TaskDto;
import com.sales.crm.model.Lead;
import com.sales.crm.model.User;
import com.sales.crm.repository.UserRepository;
import com.sales.crm.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/addTask")
    public ModelAndView addTask(Principal principal) {
        return taskService.getTask(principal);
    }

    @PostMapping("/addTask")
    public ModelAndView saveTask(@ModelAttribute("taskDto") @Valid TaskDto taskDto, BindingResult errors, Principal principal,
                                 @RequestParam(value = "lead", defaultValue = "") String lead) {
        System.out.println("lead is " + lead);
        return taskService.saveTask(taskDto, errors, principal, lead);
    }

    @GetMapping("/viewMyTasks")
    public ModelAndView viewMyTasks(Principal principal,@RequestParam(value="taskStatus",defaultValue = "")String taskStatus) {
        return taskService.getMyTasks(principal,taskStatus);
    }

    @GetMapping("/editTask/{taskId}")
    public ModelAndView editTask(@PathVariable long taskId, Principal principal) {
        System.out.println(taskId);
        return taskService.updateTask(taskId, principal);
    }

    @PostMapping("/saveTaskChanges")
    public ModelAndView saveTaskChanges(@ModelAttribute("taskDto") @Valid TaskDto taskDto, BindingResult errors, @RequestParam(value = "taskId") Long taskId,
                                        @RequestParam(value = "leadName") String leadName, Principal principal) {
        System.out.println("IN POST METHOD");
        return taskService.saveTaskChanges(taskDto, errors, taskId, leadName, principal);
    }

    @GetMapping("/deleteTask/{taskId}")
    public ModelAndView deleteTask(@PathVariable("taskId") Long taskId, Principal principal) {
        return taskService.deleteTask(taskId, principal);
    }

    @GetMapping("/deleteOtherTask/{taskId}")
    public ModelAndView deleteOtherTask(@PathVariable("taskId") Long taskId, Principal principal) {
        return taskService.deleteOtherTask(taskId, principal);
    }

    @GetMapping("/viewOtherTasks")
    public ModelAndView viewOtherTasks(Principal principal) {
        return taskService.viewOtherTasks(principal);
    }


    @GetMapping("/getLeads")
    public @ResponseBody List<Lead> getLeads(@RequestParam(value="username", required = true) String username) {
        User user = userRepository.findUserByNameAndEnabledTrue(username);
        List<Lead> leads = taskService.getLeadsByUsers(user);
        return leads;
    }

    @PostMapping("/changeTaskStatus")
    public ModelAndView changeTaskStatus(@RequestParam(value="taskId",required = true) Long taskId,@RequestParam(value="taskStatus")String status,
                                         Principal principal) {
        return taskService.changeStatus(taskId,status,principal);
    }


}
