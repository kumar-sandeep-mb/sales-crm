package com.sales.crm.controller;


import com.sales.crm.dto.LeadDto;
import com.sales.crm.service.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class LeadController {


    @Autowired
    private LeadService leadService;

    @GetMapping("/lead")
    public ModelAndView addLead(Authentication authentication) {
        return leadService.addLeadPreFunction(authentication);
    }

    @PostMapping("/save-lead")
    public ModelAndView saveLead(@ModelAttribute("leadDto") @Valid LeadDto leadDto, BindingResult bindingResult
            , Authentication authentication) {
        return leadService.addLead(leadDto, bindingResult, authentication);
    }

    @GetMapping("/add-lead")
    public ModelAndView assignLeadPreFunction(Authentication authentication) {
        return leadService.assignLeadPreFunction(authentication);
    }

    @PostMapping("/add-lead")
    public ModelAndView saveAssignedLead(@ModelAttribute("leadDto") @Valid LeadDto leadDto, BindingResult bindingResult
            , Authentication authentication) {
        return leadService.saveAssignedLead(leadDto, bindingResult, authentication);
    }

    @GetMapping("/lead/{leadId}/edit-lead")
    public ModelAndView editLead(@PathVariable(name = "leadId") int leadId) {
        return leadService.editLeadPreFunction(leadId);
    }

    @PostMapping("/lead/{leadId}/edit-lead")
    public ModelAndView editLead(@ModelAttribute("leadDto") @Valid LeadDto leadDto, BindingResult bindingResult,
                                 @PathVariable(name = "leadId") int leadId, Authentication authentication) {
        return leadService.editLead(leadDto, bindingResult, leadId, authentication);
    }

    @GetMapping("/lead-actions")
    public ModelAndView leadAction(@RequestParam("leadId") int leadId, Authentication authentication) {
        return leadService.leadAction(leadId, authentication);
    }
}
