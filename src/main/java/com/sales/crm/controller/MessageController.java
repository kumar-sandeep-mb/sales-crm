package com.sales.crm.controller;

import com.sales.crm.dto.MessageDto;
import com.sales.crm.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class MessageController {

    @Autowired
    private MessageService messageService;

    @GetMapping("/lead/{leadId}/send-message")
    public ModelAndView sendMessage(@PathVariable(name = "leadId") int leadId, Authentication authentication) {
        return messageService.sendMessagePreFunction(leadId, authentication);
    }

    @PostMapping("/lead/{leadId}/send-message")
    public ModelAndView sendMessage(@ModelAttribute("messageDto") @Valid MessageDto messageDto, BindingResult bindingResult,
                                    @PathVariable(name = "leadId") int leadId, Authentication authentication) {
        return messageService.sendMessage(messageDto, bindingResult, leadId, authentication);
    }
}
