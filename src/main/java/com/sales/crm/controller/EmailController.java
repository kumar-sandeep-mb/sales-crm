package com.sales.crm.controller;

import com.sales.crm.dto.EmailDto;
import com.sales.crm.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class EmailController {

    @Autowired
    EmailService emailService;

    @GetMapping("/lead/{leadId}/send-email")
    public ModelAndView showSendEmailPage(@PathVariable("leadId") int leadId,
                                          Authentication authentication) {
        return emailService.getSendEmailPage(leadId, authentication);
    }

    @PostMapping("/lead/{leadId}/send-email")
    public ModelAndView sendEmail(@PathVariable("leadId") int leadId, @ModelAttribute("emailDto") @Valid EmailDto emailDto,
                                  BindingResult bindingResult, Authentication authentication,
                                  @RequestParam String sentAt, @RequestParam MultipartFile multipartFile) {
        return emailService.sendEmail(leadId, emailDto, bindingResult, authentication, sentAt, multipartFile);
    }

    @GetMapping("/view-emails")
    public ModelAndView showEmails(Authentication authentication) {
        return emailService.getEmails(authentication);
    }

    @GetMapping("/view-emails/{emailId}")
    public ModelAndView showEmailByEmailId(@PathVariable("emailId") String emailId,
                                           Authentication authentication) {
        return emailService.getEmailByEmailId(emailId, authentication);
    }

}
