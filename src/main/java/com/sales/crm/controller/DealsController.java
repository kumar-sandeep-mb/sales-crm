package com.sales.crm.controller;

import com.sales.crm.dto.DealsDto;
import com.sales.crm.service.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class DealsController {

    @Autowired
    private DealService dealService;

    @GetMapping("/lead/{leadId}/add-deal")
    public ModelAndView addDealPreFunction(@PathVariable(name = "leadId") int leadId) {
        return dealService.addDealPreFunction(leadId);
    }

    @PostMapping("/lead/{leadId}/add-deal")
    public ModelAndView addDeal(@ModelAttribute("dealDto") @Valid DealsDto dealDto, BindingResult bindingResult,
                                @PathVariable(name = "leadId") int leadId) {
        return dealService.addDeal(dealDto, bindingResult, leadId);

    }

    @GetMapping("/assign-deal")
    public ModelAndView assignLeadPreFunction(Authentication authentication) {
        return dealService.assignPreFunction(authentication);
    }

    @PostMapping("/assign-deal")
    public ModelAndView assignDeal(@ModelAttribute("dealDto") @Valid DealsDto dealDto, BindingResult bindingResult
            , @RequestParam(value = "lead", defaultValue = "") String leadName, Authentication authentication) {
        return dealService.assignDeal(dealDto, bindingResult, leadName, authentication);
    }

    @GetMapping("/user-deal")
    public ModelAndView userDeal(Authentication authentication) {
        return dealService.userDealprefunctions(authentication);

    }

    @PostMapping("/user-deal")
    public ModelAndView userDeal(@ModelAttribute("dealDto") @Valid DealsDto dealDto, BindingResult bindingResult,Authentication
                                 authentication){
        return dealService.saveUserDeal(dealDto,bindingResult,authentication);
    }


    @GetMapping("/lead/{leadId}/deal/{dealId}/deal-success")
    public ModelAndView makeDeal(@PathVariable(name = "leadId") int leadId,
                                 @PathVariable(name = "dealId") int dealId,
                                 Authentication authentication) {
        return dealService.makeDeal(leadId, dealId, authentication);

    }

    @GetMapping("/all-deals")
    public ModelAndView allDeals(Authentication authentication) {
        return dealService.getAllDeals(authentication);
    }

}
