package com.sales.crm.util;

public class Constant {
    public static final int NAME_INDEX = 0;
    public static final int EMAIL_INDEX = 1;
    public static final int MOBILE_INDEX = 2;
    public static final String PROTOCOL = "imap";
    public static final String HOST = "imap.gmail.com";
    public static final String PORT = "993";
    public static final String EMAIL_USERNAME = "sandysingh1918@gmail.com";
    public static final String EMAIL_PASSWORD = "15071998";
    public static final String DEFAULT_PASSWORD = "Passcode1$";
    public static final int MINIMUM_PASSWORD_LENGTH = 8;
    public static final int MAXIMUM_PASSWORD_LENGTH = 20;
    public static final int MINIMUM_LOWER_CASE_CHARACTER_IN_PASSWORD = 1;
    public static final int MINIMUM_UPPER_CASE_CHARACTER_IN_PASSWORD = 1;
    public static final int MINIMUM_SPECIAL_CHARACTER_IN_PASSWORD = 1;
    public static final int MINIMUM_DIGITS_IN_PASSWORD = 1;
    public static final int QWERTY_SEQUENCE_LENGTH = 3;
    public static final int ALPHABETICAL_SEQUENCE_LENGTH = 3;
    public static final int NUMERICAL_SEQUENCE_LENGTH = 3;
    public static final String PASSWORD_MISMATCH_MESSAGE = "Passwords don't match";
    public static final String INVALID_EMAIL_MESSAGE = "Invalid Email";
    public static final String INVALID_PASSWORD_MESSAGE = "Invalid Password";
    public static final String COMPANY_ALREADY_EXIST_EXCEPTION_MESSAGE = "There is a Company with that name: ";
    public static final String EMAIL_REGEX = "^(.+)@(.+)$";
    public static final String SPLIT_REGEX = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String MAIL_PARSER_API_LINK = "https://files.mailparser.io/d/igjcukza";
    public static final String MAIL_PARSER_EMAIL_ADDRESS = "xwryequn@mailparser.io";
    public static final String DEFAULT_COMPANY = "mountblue";
    public static final String DEFAULT_ADMIN_NAME = "ABC";
    public static final String DEFAULT_ADMIN_EMAIL = "abc@gmail.com";
    public static final String DEFAULT_ADMIN_MOBILE = "9876543210";
    public static final String DEFAULT_ADMIN_PASSWORD = "abc";
    public static final String DEFAULT_USER_NAME = "xyz";
    public static final String DEFAULT_USER_EMAIL = "xyz@gmail.com";
    public static final String DEFAULT_USER_MOBILE = "9876543211";
    public static final String DEFAULT_USER_PASSWORD = "xyz";
    public static final String REGISTRATION_CONFIRMATION_EMAIL_SUBJECT = "Sales CRM Account Verification";
    public static final String TEAM_INVITATION_EMAIL_SUBJECT = "Sales CRM Team joining invitation";
    public static final String REGISTRATION_CONFIRMATION_EMAIL_MESSAGE = "You have registered" +
            " successfully on Sales CRM. Please Click on the link to verify your account " + " \r\n";
    public static final String TEAM_INVITATION_EMAIL_MESSAGE = "You are Invited to join the" +
            " Sales CRM Team. Click on the link to join" + " \r\n";
    public static final String TEAM_INVITATION_PAGE_URL = "/confirm-joining?token=";
    public static final String REGISTRATION_CONFIRMATION_PAGE_URL = "/confirm-registration?token=";
    public static final String SENDER = "SALESCRM";
    public static final String TOKEN = "&token=";
    public static final String RESET_PASSWORD_EMAIL_MESSAGE = "Your Reset Password Token: ";
    public static final String RESET_PASSWORD_EMAIL_SUBJECT = "Reset Password ";
    public static final String FILE_PATH = "/home/mohak/Desktop/sales-crm/";
}
