package com.sales.crm.util;

import com.sales.crm.model.Company;
import com.sales.crm.model.Role;
import com.sales.crm.model.User;
import com.sales.crm.repository.CompanyRepository;
import com.sales.crm.repository.RoleRepository;
import com.sales.crm.repository.UserRepository;
import com.sales.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.sales.crm.util.Constant.*;

@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    UserService userService;
    private boolean alreadySetup = false;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        Role adminRole = createRoleIfNotFound(ROLE_ADMIN);
        Role userRole = createRoleIfNotFound(ROLE_USER);
        Company company = createCompanyIfNotFound(DEFAULT_COMPANY);
        createUserIfNotFound(DEFAULT_ADMIN_NAME, DEFAULT_ADMIN_EMAIL, DEFAULT_ADMIN_MOBILE,
                company, DEFAULT_ADMIN_PASSWORD, adminRole);
        createUserIfNotFound(DEFAULT_USER_NAME, DEFAULT_USER_EMAIL, DEFAULT_USER_MOBILE,
                company, DEFAULT_USER_PASSWORD, userRole);
        alreadySetup = true;
    }

    @Transactional
    private final Role createRoleIfNotFound(final String name) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
        }
        role = roleRepository.save(role);
        return role;
    }

    @Transactional
    private final Company createCompanyIfNotFound(final String name) {
        Company company = companyRepository.findByName(name);
        if (company == null) {
            company = new Company(name);
        }
        company = companyRepository.save(company);
        return company;
    }

    @Transactional
    private final User createUserIfNotFound(final String name, final String email,
                                            final String mobile, final Company company,
                                            final String password, final Role role) {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setName(name);
            user.setEmail(email);
            user.setMobile(mobile);
            user.setCompany(company);
            user.setRole(role);
            user.setPassword(passwordEncoder.encode(password));
            user.setEnabled(true);
            user = userRepository.save(user);
            final String token = UUID.randomUUID().toString();
            userService.createVerificationTokenForUser(user, token);
        }
        return user;
    }

}
