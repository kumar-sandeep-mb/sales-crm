package com.sales.crm.util;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

@Component
public class TwilioSetUp {
    @Value("${twilio_sid}")
    private String twilio_sid;

    @Value("${twilio_auth}")
    private String twilio_auth;

    @Value("${twilio_number}")
    private String fromNumber;

    public String SendMessage(String toNumber, String messageContent, BindingResult bindingResult) {
        Twilio.init(twilio_sid, twilio_auth);
        try {
            Message message = Message.creator(new PhoneNumber("+91" + toNumber),
                    new PhoneNumber(fromNumber), messageContent).create();
        } catch (Exception e) {
            return "NotSent";
        }

        return "ok";

    }


}
