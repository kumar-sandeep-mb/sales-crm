package com.sales.crm.listener;

import com.sales.crm.event.OnRegistrationCompleteEvent;
import com.sales.crm.model.User;
import com.sales.crm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

import static com.sales.crm.util.Constant.*;

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {
    @Autowired
    private UserService userService;

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(final OnRegistrationCompleteEvent event) {
        final User user = event.getUser();
        final String token = UUID.randomUUID().toString();
        userService.createVerificationTokenForUser(user, token);

        final SimpleMailMessage email = constructEmailMessage(event, user, token);
        mailSender.send(email);
    }

    private final SimpleMailMessage constructEmailMessage(final OnRegistrationCompleteEvent event,
                                                          final User user, final String token) {
        final String recipientAddress = user.getEmail();

        String subject = null, confirmationUrl = null, message = null;
        if (user.getRole().getName().equals(ROLE_ADMIN)) {
            subject = REGISTRATION_CONFIRMATION_EMAIL_SUBJECT;
            confirmationUrl = event.getAppUrl() + REGISTRATION_CONFIRMATION_PAGE_URL + token;
            message = REGISTRATION_CONFIRMATION_EMAIL_MESSAGE;
        } else {
            subject = TEAM_INVITATION_EMAIL_SUBJECT;
            confirmationUrl = event.getAppUrl() + REGISTRATION_CONFIRMATION_PAGE_URL + token;
            message = TEAM_INVITATION_EMAIL_MESSAGE;
        }

        final SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + confirmationUrl);
        email.setFrom(SENDER);
        return email;
    }
}
