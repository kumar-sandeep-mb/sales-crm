package com.sales.crm.repository;

import com.sales.crm.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Integer> {
    List<Message> findByUser_Id(Long id);

    List<Message> findByLead_Id(int id);
}
