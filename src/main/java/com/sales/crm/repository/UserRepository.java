package com.sales.crm.repository;

import com.sales.crm.model.Company;
import com.sales.crm.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface UserRepository extends JpaRepository<User, Long> {


    User findByEmail(String email);

    public User findByEmailIgnoreCase(String email);

    public User findUserByEmail(String email);

    public List<User> findUsersByCompanyAndEnabledTrue(Company company);

    public User findUserByNameAndEnabledTrue(String username);

    public List<User> findByCompany_Name(String company);

    public User findUserByIdAndEnabledTrue(long ID);

}
