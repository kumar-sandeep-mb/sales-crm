package com.sales.crm.repository;

import com.sales.crm.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {

    public Company findByName(String name);
}
