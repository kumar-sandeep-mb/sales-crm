package com.sales.crm.repository;

import com.sales.crm.model.Lead;
import com.sales.crm.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LeadRepository extends JpaRepository<Lead, Integer> {
    Lead findById(int id);

    Lead findByEmailIgnoreCase(String email);

    Lead findByMobile(String mobile);

    List<Lead> findByEnabledTrue();

    List<Lead> findByUser_Company_Name(String name);

    List<Lead> findByUser_Id(Long id);

    List<Lead> findByUser_IdOrderByPointsDesc(Long id);

    List<Lead> findLeadsByUser(User user);

    public Lead findLeadByName(String leadName);
}
