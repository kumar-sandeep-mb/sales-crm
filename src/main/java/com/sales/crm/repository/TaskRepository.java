package com.sales.crm.repository;

import com.sales.crm.model.Task;
import com.sales.crm.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    public Task findTaskById(Long id);

    List<Task> findTasksByUserOrderByCreatedAtDesc(User user);

    List<Task> findTasksByUserOrderByCreatedAtAsc(User user);


    List<Task> findAllByUser(User user);
    List<Task> findByLead_Id(int id);

    List<Task> findTaskByUserAndTaskNameLikeIgnoreCase(User user,String taskName);
    List<Task> findTasksByUserAndTaskStatus(User user,String taskStatus);

}
