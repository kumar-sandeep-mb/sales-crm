package com.sales.crm.repository;

import com.sales.crm.model.CustomEmail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmailRepository extends JpaRepository<CustomEmail, Long> {
    List<CustomEmail> findAllBySender(String sender);
    List<CustomEmail> findByReceiver(String reciver);
}
