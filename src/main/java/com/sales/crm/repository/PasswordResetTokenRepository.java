package com.sales.crm.repository;

import com.sales.crm.model.PasswordResetToken;
import com.sales.crm.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {

    public PasswordResetToken findByToken(String token);

    public void deleteAllByUser(User user);
}
