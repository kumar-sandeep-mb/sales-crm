package com.sales.crm.repository;

import com.sales.crm.model.Company;
import com.sales.crm.model.Deal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DealRepository extends JpaRepository<Deal, Integer> {

    public List<Deal> findByLead_Id(int id);

    public Deal findById(int id);

    public List<Deal> findByLead_IdAndStatusTrue(int lid);

    public List<Deal> findByLead_IdAndStatusFalse(int lid);

    public List<Deal> findByLead_User_IdAndStatusTrue(Long uid);

    public List<Deal> findByLead_User_IdAndStatusFalse(Long uid);

    public List<Deal> findByLead_User_Company_Name(String company);

    public List<Deal> findByLead_User_Id(Long userId);

    public List<Deal> findByLead_User_CompanyAndStatusTrue(Company company);

    public List<Deal> findByLead_User_CompanyAndStatusFalse(Company company);


}
