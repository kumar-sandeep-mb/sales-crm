package com.sales.crm.exception;

import static com.sales.crm.util.Constant.COMPANY_ALREADY_EXIST_EXCEPTION_MESSAGE;
import static org.springframework.security.core.SpringSecurityCoreVersion.SERIAL_VERSION_UID;

public class CompanyAlreadyExistException extends RuntimeException {
    private static final long serialVersionUID = SERIAL_VERSION_UID;

    public CompanyAlreadyExistException(final String companyName) {
        super(COMPANY_ALREADY_EXIST_EXCEPTION_MESSAGE + companyName);
    }
}
