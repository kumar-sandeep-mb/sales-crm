package com.sales.crm.exception;

import static org.springframework.security.core.SpringSecurityCoreVersion.SERIAL_VERSION_UID;

public final class UserAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = SERIAL_VERSION_UID;

    public UserAlreadyExistException(final String message) {
        super(message);
    }
}
