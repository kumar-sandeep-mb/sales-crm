package com.sales.crm.service;

import com.sales.crm.dto.MessageDto;
import com.sales.crm.model.Lead;
import com.sales.crm.model.Message;
import com.sales.crm.model.User;
import com.sales.crm.repository.MessageRepository;
import com.sales.crm.util.TwilioSetUp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Service
public class MessageService {

    @Autowired
    private LeadService leadService;

    @Autowired
    private UserService userService;
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private TwilioSetUp twilioSetUp;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public List<Message> findMessageByLeadId(int leadId) {
        return messageRepository.findByLead_Id(leadId);
    }

    public List<Message> getByUser_Id(Long leadId) {
        return messageRepository.findByUser_Id(leadId);
    }

    public List<Message> getByLead_Id(int leadId) {
        return messageRepository.findByLead_Id(leadId);
    }

    public ModelAndView sendMessagePreFunction(int leadId, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView();

        logger.info("renderring the send Message prefunction to get the message page");
        modelAndView.addObject("lead",leadService.findLead(leadId));
        modelAndView.addObject("user",userService.findUserByMail(authentication.getName()));
        modelAndView.addObject("success","");
        modelAndView.addObject("messageDto",new MessageDto());
        modelAndView.setViewName("sendMessage");
        return modelAndView;
    }

    public ModelAndView sendMessage(MessageDto messageDto, BindingResult bindingResult, int leadId, Authentication authentication) {
        logger.info("renderring the message function to send the messages to lead");
        Lead lead = leadService.findLead(leadId);
        User user = userService.findUserByMail(authentication.getName());
        ModelAndView modelAndView = new ModelAndView();
        if(bindingResult.hasErrors()){
            logger.warn("validation failure in send Message function");
            modelAndView.addObject("user",user);
            modelAndView.addObject("success","");
            modelAndView.addObject("lead",lead);
            modelAndView.setViewName("sendMessage");
            return modelAndView;
        }

        final Message message =new Message();
        message.setUser(user);
        message.setLead(lead);
        message.setMessage(messageDto.getMessage());
        String status = twilioSetUp.SendMessage(lead.getMobile(),messageDto.getMessage(),bindingResult);
        if(!status.equals("ok")){
            logger.error("lead number or network problem");
            modelAndView.addObject("user",user);
            modelAndView.addObject("success","Number Not varified");
            modelAndView.addObject("lead",lead);
            modelAndView.setViewName("sendMessage");
            return modelAndView;

        } else {
            messageRepository.save(message);
            logger.info("message send to the lead from twilio");
            modelAndView.addObject("user",user);
            modelAndView.addObject("success","Message Sent");
            modelAndView.addObject("messageDto",new MessageDto());
            modelAndView.addObject("lead",lead);
            modelAndView.setViewName("sendMessage");
            return modelAndView;

        }
    }

}
