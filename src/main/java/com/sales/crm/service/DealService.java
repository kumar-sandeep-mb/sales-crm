package com.sales.crm.service;

import com.sales.crm.dto.DealsDto;
import com.sales.crm.model.Deal;
import com.sales.crm.model.Lead;
import com.sales.crm.model.User;
import com.sales.crm.repository.DealRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Service
public class DealService {

    @Autowired
    private DealRepository dealRepository;
    @Autowired
    private LeadService leadService;
    @Autowired
    private UserService userService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public ModelAndView addDealPreFunction(int leadId) {
        logger.info("renderring the add deal prefunction to get the page");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("dealDto", new DealsDto());
        modelAndView.addObject("message", "");
        modelAndView.setViewName("addDeal");
        return modelAndView;
    }

    public ModelAndView addDeal(DealsDto dealDto, BindingResult bindingResult, int leadId) {
        logger.info("renderring the add lead function to save the deal");
        ModelAndView modelAndView = new ModelAndView();
        Lead lead = leadService.findLead(leadId);

        if(bindingResult.hasErrors()){
            logger.warn("validation failure in deal dto");
            modelAndView.addObject("message","");
            modelAndView.setViewName("addDeal");
            return modelAndView;
        }
        final Deal deals = new Deal();
        deals.setDealName(dealDto.getDealName());
        deals.setLead(lead);
        deals.setStatus(false);
        deals.setWorth(dealDto.getWorth());
        dealRepository.save(deals);
        modelAndView.addObject("message","Deal Added");
        logger.info("deal saved to database successfully");
        modelAndView.addObject("message","Deal Added Succesfully");
        modelAndView.addObject("dealDto",new DealsDto());
        modelAndView.setViewName("addDeal");
        return modelAndView;
    }

    public ModelAndView assignPreFunction(Authentication authentication) {
        logger.info("renderring the assign the deal prefunction to get the page");
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getByEmailIgnoreCase(authentication.getName());
        modelAndView.addObject("users", userService.findByCompany(user.getCompany()));
        modelAndView.addObject("dealDto", new DealsDto());
        modelAndView.addObject("error", false);
        modelAndView.addObject("message", false);
        modelAndView.setViewName("assigndeal");
        return modelAndView;
    }

    public ModelAndView assignDeal(DealsDto dealDto, BindingResult bindingResult, String leadName, Authentication authentication) {

        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getByEmailIgnoreCase(authentication.getName());
        System.out.println(leadName);

        if(bindingResult.hasErrors() ){
            logger.warn("validation failure in assigndeals");
            modelAndView.addObject("users",userService.findByCompany(user.getCompany()));
            modelAndView.setViewName("assigndeal");
            return modelAndView;
        }
        if(leadName.isEmpty()){
            logger.warn("validation failure for empty lead name");
            modelAndView.addObject("users",userService.findByCompany(user.getCompany()));
            modelAndView.addObject("error",true);
            modelAndView.setViewName("assigndeal");
            return modelAndView;
        }
        Lead lead = leadService.findLeadByName(leadName);
        System.out.println(lead.getName());
        Deal deal = new Deal();
        deal.setStatus(false);
        deal.setDealName(dealDto.getDealName());
        deal.setLead(lead);
        deal.setWorth(dealDto.getWorth());
        dealRepository.save(deal);
        logger.info("deal added succesfully to deal database");
        modelAndView.addObject("users",userService.findByCompany(user.getCompany()));
        modelAndView.addObject("message",true);
        modelAndView.addObject("dealDto",new DealsDto());
        modelAndView.setViewName("assigndeal");
        return modelAndView;
    }


    public ModelAndView makeDeal(int leadId, int dealId,Authentication authentication) {
        logger.info("renderring the make deal function to make a deal with lead");
        ModelAndView modelAndView = new ModelAndView();
        Deal deal = dealRepository.findById(dealId);
        deal.setStatus(true);
        Lead lead = leadService.findLead(leadId);
        lead.setPoints(lead.getPoints() + 2);
        leadService.saveLead(lead);
        dealRepository.save(deal);
        if (userService.getByEmailIgnoreCase(authentication.getName()).getRole().getName().equals("ROLE_USER")) {
            modelAndView.setViewName("redirect:/user");
            return modelAndView;
        }
        modelAndView.setViewName("redirect:/all-deals");
        return modelAndView;
    }

    public ModelAndView getAllDeals(Authentication authentication) {
        logger.info("renderring the getAlldeals function to obtain every deals");
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.findUserByMail(authentication.getName());

        if(user.getRole().getName().equals("ROLE_USER")){
            logger.info("provide only the user deals");
            List<Deal> successDeals = dealRepository.findByLead_User_IdAndStatusTrue(user.getId());
            List<Deal> pendingDeals = dealRepository.findByLead_User_IdAndStatusFalse(user.getId());
            modelAndView.addObject("successDeals", successDeals);
            modelAndView.addObject("pendingDeals", pendingDeals);
            modelAndView.setViewName("dealPage");
            return modelAndView;
        }
        else{
            logger.info("provides every deal with the company");
            List<Deal> successDeals = dealRepository.findByLead_User_CompanyAndStatusTrue(user.getCompany());
            List<Deal> pendingDeals = dealRepository.findByLead_User_CompanyAndStatusFalse(user.getCompany());
            modelAndView.addObject("successDeals", successDeals);
            modelAndView.addObject("pendingDeals", pendingDeals);
            modelAndView.setViewName("alldeals");
            return modelAndView;
        }

    }

    public ModelAndView userDealprefunctions(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getByEmailIgnoreCase(authentication.getName());
        List<Lead> leads = leadService.findLeadsByUser(user);
        modelAndView.addObject("user",user);
        modelAndView.addObject("leads",leads);
        modelAndView.addObject("dealDto",new DealsDto());
        modelAndView.addObject("message",false);
        modelAndView.setViewName("dealbyuser");
        return modelAndView;
    }


    public ModelAndView saveUserDeal(DealsDto dealDto, BindingResult bindingResult, Authentication authentication) {
        logger.info("renderring the add lead function to save the deal");
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.getByEmailIgnoreCase(authentication.getName());
        modelAndView.addObject("user",user);
        List<Lead> leads = leadService.findLeadsByUser(user);
        modelAndView.addObject("leads",leads);
        if(bindingResult.hasErrors()){
            logger.warn("validation failure in deal dto");
            modelAndView.addObject("message",false);
            modelAndView.setViewName("dealbyuser");
            return modelAndView;
        }
        if(dealDto.getLead()==null){
            logger.warn("Select the lead or empty lead object");
            bindingResult.rejectValue("lead", "error.lead", "PLEASE ASSIGN A LEAD TO DEAL");
            modelAndView.addAllObjects(bindingResult.getModel());
            modelAndView.addObject("message",false);
            modelAndView.setViewName("dealbyuser");
            return modelAndView;
        }
        final Deal deal = new Deal();
        deal.setDealName(dealDto.getDealName());
        deal.setStatus(false);
        deal.setLead(dealDto.getLead());
        deal.setWorth(dealDto.getWorth());
        dealRepository.save(deal);
        logger.info("deal saved to database successfully");
        modelAndView.addObject("message",true);
        modelAndView.addObject("dealDto",new DealsDto());
        modelAndView.setViewName("dealbyuser");
        return modelAndView;

    }

    public List<Deal> getDealsByUser(Long userId) {
        return dealRepository.findByLead_User_Id(userId);
    }

    public List<Deal> getByLead_User_IdAndStatusTrue(Long userId) {
        return dealRepository.findByLead_User_IdAndStatusTrue(userId);
    }

    public List<Deal> getByLead_User_IdAndStatusFalse(Long userId) {
        return dealRepository.findByLead_User_IdAndStatusFalse(userId);
    }

    public List<Deal> findLeadByLeadId(int leadId) {
        return dealRepository.findByLead_Id(leadId);
    }

    public List<Deal> findDealStatusTrue(int leadId) {
        return dealRepository.findByLead_IdAndStatusTrue(leadId);
    }

    public List<Deal> findDealStatusFalse(int leadId) {
        return dealRepository.findByLead_IdAndStatusFalse(leadId);
    }



}

