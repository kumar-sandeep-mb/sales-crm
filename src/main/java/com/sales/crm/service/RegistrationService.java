package com.sales.crm.service;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.sales.crm.dto.*;
import com.sales.crm.event.OnRegistrationCompleteEvent;
import com.sales.crm.model.Company;
import com.sales.crm.model.PasswordResetToken;
import com.sales.crm.model.User;
import com.sales.crm.model.UserVerificationToken;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.security.Principal;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.sales.crm.util.Constant.*;

@Service
public class RegistrationService {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Autowired
    CompanyService companyService;
    @Autowired
    JavaMailSender mailSender;
    @Autowired
    private ApplicationEventPublisher eventPublisher;
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserVerificationTokenService userVerificationTokenService;
    @Autowired
    private PasswordResetTokenService passwordResetTokenService;

    public ModelAndView getSignUpPage(String view, Principal principal) {
        LOGGER.debug("Rendering showSignUpPage.");
        final UserDto userDto = new UserDto();
        ModelAndView modelAndView = new ModelAndView();
        if (principal != null) {
            System.out.println(principal.getName());
            userDto.setPassword("Passcode1$");
            userDto.setMatchingPassword("Passcode1$");
        }
        modelAndView.addObject("userDto", userDto);
        modelAndView.setViewName(view);
        return modelAndView;
    }

    public ModelAndView registerNewUser(UserDto userDto, BindingResult bindingResult,
                                        HttpServletRequest httpServletRequest, Principal principal) {
        LOGGER.debug("Rendering registerNewManager");
        ModelAndView modelAndView = new ModelAndView();
        if (principal == null) {
            if (bindingResult.hasErrors()) {
                modelAndView.setViewName("registerPage");
                return modelAndView;
            }
            Company company = companyService.getByName(userDto.getCompanyDto().getName());
            if (company != null) {
                bindingResult.rejectValue("companyDto.name", "error.user", "Company or branch " +
                        "already exits");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.setViewName("registerPage");
                return modelAndView;
            }
            User user = userService.getByEmailIgnoreCase(userDto.getEmail());
            if (user != null) {
                bindingResult.rejectValue("email", "error.user", "Mail Already exits you" +
                        " can't create a account on this email");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.setViewName("registerPage");
                return modelAndView;
            }
        }
        if (principal != null) {
            if (bindingResult.hasErrors()) {
                modelAndView.setViewName("addUser");
                return modelAndView;
            }
            User user = userService.getByEmailIgnoreCase(userDto.getEmail());
            if (user != null) {
                bindingResult.rejectValue("email", "error.user", "Mail Already exits you" +
                        " can't create a account on this email");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.setViewName("addUser");
                return modelAndView;
            }
        }

        User registered = null;
        if (principal == null) {
            registered = userService.registerNewUser(userDto);
        } else {
            LOGGER.info("TEAM MEMBER REGISTRATION");
            registered = userService.addUser(userDto, principal);

        }
        if (registered == null) {
            modelAndView.addObject("successMessage", "user already exists");
            LOGGER.error("Error occurred in Registration details, user with this email already registered:" +
                    " {}", userDto);
        } else {
            try {
                String appUrl = "http://" + httpServletRequest.getServerName() + ":" +
                        httpServletRequest.getServerPort() + httpServletRequest.getContextPath();

                eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered,
                        httpServletRequest.getLocale(), appUrl));
                modelAndView.addObject("message", "registered successfully");
            } catch (final Exception ex) {
                LOGGER.error("Unable to register user", ex);
                modelAndView.setViewName("registerPage");
                modelAndView.addObject("message", "connect to internet to send the mail");
                return modelAndView;
            }
            if (principal != null) {
                modelAndView.setViewName("addUser");
                return modelAndView;
            }
            modelAndView.setViewName("registration-successful");
            LOGGER.debug("registered successfully: {}", registered);

            modelAndView.addObject("userEmail", registered.getEmail());
        }
        return modelAndView;
    }

    public ModelAndView resendVerificationMail(HttpServletRequest request, String userEmail) {
        LOGGER.debug("rendering resend Verification mail");
        User registered = userService.getByEmailIgnoreCase(userEmail);
        ModelAndView modelAndView = new ModelAndView();
        if (registered == null) {
            modelAndView.addObject("successMessage", "user does not exists");
            modelAndView.setViewName("bad-request");
            LOGGER.error("User does not exist with email : {}", userEmail);
        } else if (registered.isEnabled()) {
            modelAndView.addObject("successMessage", "user already verified");
            modelAndView.setViewName("bad-request");
            LOGGER.debug("user already verified {}", registered);
        } else {
            UserVerificationToken existingToken = userVerificationTokenService.getByUser(registered);
            final UserVerificationToken newToken = userService.generateNewUserVerificationToken(existingToken.getToken());
            try {
                final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
                mailSender.send(constructMail(userEmail, SENDER, REGISTRATION_CONFIRMATION_EMAIL_SUBJECT,
                        REGISTRATION_CONFIRMATION_EMAIL_MESSAGE + appUrl +
                                REGISTRATION_CONFIRMATION_PAGE_URL + newToken.getToken()));
                modelAndView.addObject("userEmail", userEmail);
                modelAndView.addObject("successMessage", "verification mail sent");
                modelAndView.setViewName("registration-successful");
                LOGGER.debug("verification mail resent successfully to mail: {}", userEmail);
            } catch (final Exception exception) {
                modelAndView.addObject("successMessage", exception.getLocalizedMessage());
                modelAndView.setViewName("bad-request");
                LOGGER.error(exception.getLocalizedMessage(), exception);
            }

            modelAndView.addObject("userDto", new UserDto());
            return modelAndView;


//        } catch(UserAlreadyExistException userAlreadyExistException){
//            LOGGER.error(userAlreadyExistException.getMessage());
//            modelAndView.addObject("message",userAlreadyExistException.getMessage());
//            return modelAndView;
//        } catch (CompanyAlreadyExistException companyAlreadyExistException) {
//            LOGGER.error(companyAlreadyExistException.getMessage());
//            modelAndView.addObject("message",companyAlreadyExistException.getMessage());
//            return modelAndView;
//        } catch (final Exception exception) {
//            LOGGER.error("Unable to register user", exception);
//            modelAndView.addObject("successMessage",
//                    "connect to internet to send the mail");
//            return modelAndView;
        }
        return modelAndView;
    }


    public String confirmRegister(HttpServletRequest request, Model model, String token) {
        LOGGER.debug("rendering registration confirmation");
        final Locale locale = request.getLocale();
        final UserVerificationToken userVerificationToken = userVerificationTokenService.getByToken(token);
        if (userVerificationToken == null) {
            model.addAttribute("message", "invalid token");
            LOGGER.error("invalid verification token: {}", token);
            return "redirect:/badUser.html?lang=" + locale.getLanguage();
        }
        final User user = userVerificationToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((userVerificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            model.addAttribute("message", "token expired");
            model.addAttribute("expired", true);
            model.addAttribute("token", token);
            LOGGER.error("token expired: {}", token);
            return "redirect:/badUser.html?lang=" + locale.getLanguage();
        }
        if (user.getRole().getName().equals("ROLE_ADMIN")) {
            user.setEnabled(true);
            userService.saveRegisteredUser(user);
            model.addAttribute("message", "Account verified");
            LOGGER.debug("Author verified with token: {}", token);
            return "registrationConfirm";
        } else {
            PasswordDto dto = new PasswordDto();
            dto.setEmail(user.getEmail());
            System.out.println(dto.getEmail());
            model.addAttribute("error", false);
            model.addAttribute("passwordDto", dto);
            return "setUserPassword";
        }
    }

    public String saveUser(PasswordDto dto) {
        userService.confirmUser(dto);
        return "redirect:/";
    }

    public String uploadFile(MultipartFile file, Model model, Principal principal, HttpServletRequest httpServletRequest) {
        int count_incorrect = 0, count_duplicate = 0, totalUsers = 0;
        if (file.isEmpty()) {
            model.addAttribute("message", "Please select a CSV file to upload.");
            model.addAttribute("status", false);
            return "importUsers";
        } else {

            try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
                CsvToBean<UserCsvDto> csvToBean = new CsvToBeanBuilder(reader)
                        .withType(UserCsvDto.class)
                        .withIgnoreLeadingWhiteSpace(true)
                        .build();

                List<UserCsvDto> users = csvToBean.parse();
                totalUsers = users.size();
                List<UserCsvDto> incorrectUsers = new ArrayList<>();
                User registered = null;

                for (UserCsvDto user : users) {

                    if (!Pattern.compile("[a-zA-Z]+").matcher(user.getName()).matches() || !Pattern.compile("[a-zA-Z0-9_.-]+@[A-Za-z]+\\.[a-z]+").matcher(user.getEmail())
                            .matches() || !Pattern.compile("[6-9][0-9]{9}").matcher(user.getMobile()).matches()) {
                        LOGGER.info("Some contacts have incorrect details." + user.getName());
                        model.addAttribute("incorrectData", true);
                        count_incorrect++;
                        model.addAttribute("warning", count_incorrect + " contacts have incorrect details.");
                        incorrectUsers.add(user);
                        continue;
                    }

                    if (userService.emailExists(user.getEmail())) {
                        LOGGER.info("some emails already exist in the table." + user.getEmail());
                        model.addAttribute("emailExists", true);
                        count_duplicate++;
                        model.addAttribute("info", count_duplicate + " contacts already exist.");
                        incorrectUsers.add(user);
                        continue;
                    }

                    UserDto userDto = new UserDto();
                    userDto.setName(user.getName());
                    userDto.setEmail(user.getEmail());
                    userDto.setMobile(user.getMobile());
                    userDto.setPassword("Passcode1$");
                    userDto.setMatchingPassword("Passcode1$");
                    registered = userService.addUser(userDto, principal);
                    sendMail(registered, httpServletRequest);
                }

                if(!incorrectUsers.isEmpty()) {
                    Writer writer = new FileWriter("errors.csv");
                    StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
                    beanToCsv.write(incorrectUsers);
                    writer.close();
                    model.addAttribute("badData",true);
                }

                model.addAttribute("status",true);
            } catch (Exception ex) {
                model.addAttribute("message", "An error occurred while processing the CSV file.");
                model.addAttribute("status", false);

                model.addAttribute("emailExists",false);
                model.addAttribute("incorrectData",false);
                model.addAttribute("badData",false);
                return "importUsers";
            }
        }

            model.addAttribute("success", true);
            model.addAttribute("message", (totalUsers-count_duplicate-count_incorrect)+" Invites sent.");

        return "importUsers";
    }

    public ModelAndView showForgotPasswordPage() {
        LOGGER.debug("rendering showForgotPasswordPage");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("forgot-password");
        return modelAndView;
    }

    public ModelAndView sendForgotPasswordMail(String userEmail) {
        LOGGER.debug("rendering resetPassword");
        ModelAndView modelAndView = new ModelAndView();
        final User user = userService.getUserByEmail(userEmail);
        if (user == null) {
            modelAndView.addObject("successMessage", "user not found");
            LOGGER.error("User does not exist with email: {}", userEmail);
            modelAndView.setViewName("forgot-password");
        } else {
            passwordResetTokenService.deleteAllByUser(userService.getByEmailIgnoreCase(userEmail));
            final String token = UUID.randomUUID().toString();
            userService.createPasswordResetTokenForUser(user, token);
            try {
                mailSender.send(constructMail(userEmail, SENDER, RESET_PASSWORD_EMAIL_SUBJECT,
                        RESET_PASSWORD_EMAIL_MESSAGE + token));
                LOGGER.debug("Password reset verification mail sent to mail: {}", userEmail);
                modelAndView.addObject("successMessage", "Enter Token sent to your mail");
                modelAndView.addObject("resetPasswordDto", new ResetPasswordDto());
                modelAndView.setViewName("reset-password");
            } catch (final Exception exception) {
                LOGGER.error(exception.getLocalizedMessage(), exception);
                exception.printStackTrace();
                modelAndView.addObject("message", exception.getLocalizedMessage());
                modelAndView.setViewName("forgot-password");
            }
        }
        return modelAndView;
    }

    public ModelAndView resetPassword(ResetPasswordDto resetPasswordDto,
                                      BindingResult bindingResult) {
        LOGGER.debug("rendering updatePassword with resetPasswordDto: {}", resetPasswordDto);
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            LOGGER.error("invalid password {}", resetPasswordDto);
            modelAndView.addObject("successMessage", "invalid password");
            modelAndView.addObject("resetPasswordDto", resetPasswordDto);
            modelAndView.setViewName("reset-password");
            return modelAndView;
        }
        if (!resetPasswordDto.getPassword().equals(resetPasswordDto.getMatchingPassword())) {
            LOGGER.error("password mismatch {}", resetPasswordDto);
            modelAndView.setViewName("reset-password");
            modelAndView.addObject("resetPasswordDto", resetPasswordDto);
            modelAndView.addObject("successMessage", "password mismatch");
            return modelAndView;
        }
        final PasswordResetToken passwordResetToken =
                userService.getPasswordResetToken(resetPasswordDto.getPasswordResetToken());
        if (passwordResetToken == null) {
            modelAndView.addObject("successMessage", "invalid token");
            LOGGER.error("invalid token: {}", resetPasswordDto.getPasswordResetToken());
            modelAndView.setViewName("reset-password");
            modelAndView.addObject("resetPasswordDto", resetPasswordDto);
            return modelAndView;
        }
        final Calendar cal = Calendar.getInstance();
        if ((passwordResetToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            modelAndView.addObject("successMessage", "token expired");
            LOGGER.error("token expired: {}", resetPasswordDto.getPasswordResetToken());
            modelAndView.setViewName("reset-password");
            modelAndView.addObject("resetPasswordDto", resetPasswordDto);
            return modelAndView;
        }
        final User user = passwordResetToken.getUser();
        userService.changeUserPassword(userService.getByEmailIgnoreCase(
                user.getEmail()), resetPasswordDto.getPassword());
        modelAndView.addObject("successMessage", "password changed");
        modelAndView.setViewName("password-changed-successfully");
        userService.deletePasswordResetToken(passwordResetToken);
        LOGGER.debug("password changed for user {}", user);
        return modelAndView;
    }

    public ModelAndView showUserSettingPage(Authentication authentication) {
        User user = userService.getUserByEmail(authentication.getName());
        CompanyDto companyDto = new CompanyDto();
        companyDto.setName(user.getCompany().getName());
        UserDto userDto = new UserDto();
        userDto.setCompanyDto(companyDto);
        userDto.setName(user.getName());
        userDto.setEmail(user.getEmail());
        userDto.setMobile(user.getMobile());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("userDto", userDto);
        modelAndView.setViewName("setting");
        return modelAndView;
    }

    public ModelAndView updateUserSetting(HttpServletRequest request,
                                          BindingResult bindingResult, Authentication authentication,
                                          UserDto userDto) {
        LOGGER.debug("rendering updateUserSetting");
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            modelAndView.addObject("successMessage", "invalid details");
            LOGGER.error("empty string in name {}", userDto);
            modelAndView.setViewName("setting");
            modelAndView.addObject("userDto", userDto);
            return modelAndView;
        }
        User registered = userService.getUserByEmail(authentication.getName());
        registered.setName(userDto.getName());
        registered.setMobile(userDto.getMobile());
        registered.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userService.saveRegisteredUser(registered);
        UserVerificationToken existingToken = userVerificationTokenService.getByUser(registered);
        final UserVerificationToken newToken = userService.generateNewUserVerificationToken(existingToken.getToken());
        final User user = userService.getUser(newToken.getToken());
        if (!registered.getEmail().equals(userDto.getEmail())) {
            registered.setEmail(userDto.getEmail());
            registered.setEnabled(false);
            try {
                final String appUrl = "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
                mailSender.send(constructMail(user.getEmail(), SENDER, REGISTRATION_CONFIRMATION_EMAIL_SUBJECT,
                        REGISTRATION_CONFIRMATION_EMAIL_MESSAGE + appUrl +
                                REGISTRATION_CONFIRMATION_PAGE_URL + newToken.getToken()));
            } catch (final MailAuthenticationException mailAuthenticationException) {
                LOGGER.error("MailAuthenticationException", mailAuthenticationException);
                modelAndView.addObject("successMessage", mailAuthenticationException.getLocalizedMessage());
                return modelAndView;
            } catch (final Exception exception) {
                LOGGER.error(exception.getLocalizedMessage(), exception);
                modelAndView.addObject("successMessage", exception.getLocalizedMessage());
                return modelAndView;
            }
            modelAndView.addObject("successMessage", "Updated Successfully and " +
                    "verification link sent to your mail");
            LOGGER.debug("Updated ser settings successfully");
        } else {
            modelAndView.addObject("successMessage", "updated successfully");
            LOGGER.debug("Updated user settings successfully");
        }
        modelAndView.addObject("userDto", userDto);
        return modelAndView;
    }

    public void sendMail(User registered, HttpServletRequest httpServletRequest) {
        try {
            String appUrl = "http://" + httpServletRequest.getServerName() + ":" +
                    httpServletRequest.getServerPort() + httpServletRequest.getContextPath();

            eventPublisher.publishEvent(new OnRegistrationCompleteEvent(registered,
                    httpServletRequest.getLocale(), appUrl));
        } catch (final Exception ex) {
            LOGGER.error("Unable to register user", ex);
        }
    }

    private SimpleMailMessage constructMail(final String receiver, final String sender,
                                            final String subject, final String message) {
        final SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(message);
        simpleMailMessage.setTo(receiver);
        simpleMailMessage.setFrom(sender);
        return simpleMailMessage;
    }




    public void exportCsv(HttpServletRequest request, HttpServletResponse response) {
        try {
            String fileName="errors.csv";
            String filePathToBeServed = FILE_PATH;
            File fileToDownload = new File(filePathToBeServed+fileName);

            InputStream inputStream = new FileInputStream(fileToDownload);
            response.setContentType("application/csv");
            response.setHeader("Content-Disposition", "attachment; filename="+fileName);
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
            inputStream.close();
        } catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }





}
