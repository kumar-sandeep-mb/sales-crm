package com.sales.crm.service;

import com.sales.crm.model.Company;
import com.sales.crm.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    public Company save(Company company) {
        return companyRepository.save(company);
    }

    public Company getByName(String companyName) {
        return companyRepository.findByName(companyName);
    }
}
