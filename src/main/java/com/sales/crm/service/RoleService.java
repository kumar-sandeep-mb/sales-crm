package com.sales.crm.service;

import com.sales.crm.model.Role;
import com.sales.crm.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Role getByName(String roleName) {
        return roleRepository.findByName(roleName);
    }
}
