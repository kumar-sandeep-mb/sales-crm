package com.sales.crm.service;

import com.sales.crm.dto.TaskDto;
import com.sales.crm.model.Lead;
import com.sales.crm.model.Task;
import com.sales.crm.model.User;
import com.sales.crm.repository.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService {

    Logger logs = LoggerFactory.getLogger(getClass());

    @Autowired
    TaskRepository taskRepository;

    @Autowired
    UserService userService;

    @Autowired
    LeadService leadService;

    public ModelAndView getTask(Principal principal) {
        ModelAndView model = new ModelAndView();
        if (principal == null) {
            model.setViewName("invalidSession");
            return model;
        }
        User user = userService.findUserByEmail(principal.getName());
        List<User> users = new ArrayList<>();
        List<Lead> leads = new ArrayList<>();

        if (user.getRole().getName().equals("ROLE_ADMIN")) {
            users = getUsersByEmail(principal.getName());
        } else {
            users.add(user);
//            leads = getLeadsByUsers(user);
//            model.addObject("leads",leads);
        }
        TaskDto taskDto = new TaskDto();
        model.addObject("taskDto", taskDto);
        model.addObject("users", users);
        model.addObject("leadError", false);
        model.setViewName("addTask");
        return model;
    }


    public ModelAndView saveTask(TaskDto taskDto, BindingResult errors, Principal principal, String leadName) {
        User loggedInUser = userService.findUserByEmail(principal.getName());
        ModelAndView model = new ModelAndView();
        if (principal == null) {
            logs.info("UNAUTHORIZED ACCESS");
            model.setViewName("invalidSession");
            return model;
        }
        if (errors.hasErrors()) {
            logs.info("ERROR IN SAVE TASK METHOD.");
            if (loggedInUser.getRole().getName().equals("ROLE_ADMIN")) {
                List<User> users = getUsersByEmail(principal.getName());
                model.addObject("users", users);
                model.setViewName("addTask");
                return model;
            } else {
                model.addObject("users", loggedInUser);
                model.setViewName("addTask");
                return model;
            }
        }
        if (leadName.isEmpty() || leadName == null) {
            System.out.println("ERROR IN SAVE TASK METHOD. EMPTY LEAD FIELD");

            if(loggedInUser.getRole().getName().equals("ROLE_ADMIN")) {
                List<User> users = getUsersByEmail(principal.getName());
                model.addObject("users", users);
            }
            else{
                model.addObject("users",loggedInUser);
            }
            model.addObject("leadError",true);
            model.setViewName("addTask");
            return model;
        }
        logs.info("OUTSIDE ERROR ZONE");
        User user = userService.findUserByName(taskDto.getUserName());
        Lead lead = leadService.findLeadByName(leadName);
        Task task = new Task();
        task.setTaskName(taskDto.getTaskName());
        task.setTaskDescription(taskDto.getTaskDescription());
        task.setTaskTime(taskDto.getTaskTime());
        task.setUser(user);
        task.setLead(lead);
        task.setTaskStatus("ASSIGNED");
        taskRepository.save(task);
        logs.info("TASK SAVED SUCCESSFULLY.");

        TaskDto taskdto = new TaskDto();
        List<User> users = getUsersByEmail(principal.getName());
        model.addObject("users", users);
        model.addObject("taskDto", taskdto);
        model.addObject("taskSaved", true);
//        User loggedInUser = userService.findUserByName(principal.getName());
//        model.addObject("loggedInUser",loggedInUser);
        model.setViewName("addTask");
        return model;
    }

    public List<User> getUsersByEmail(String email) {
        User user = userService.findUserByEmail(email);
        List<User> users = userService.findUserByCompany(user.getCompany());
        return users;
    }

    public List<Lead> getLeadsByUsers(User user) {
        List<Lead> leads = leadService.findLeadsByUser(user);
        return leads;
    }

    public List<Task> findTasksByUserAndTaskStatus(User user,String taskStatus) {
        return taskRepository.findTasksByUserAndTaskStatus(user,taskStatus);
    }

    public List<Task> findTasksByUserDesc(User user) {
        return taskRepository.findTasksByUserOrderByCreatedAtDesc(user);
    }

    public List<Task> findTasksByUserAsc(User user) {
        return taskRepository.findTasksByUserOrderByCreatedAtAsc(user);
    }

    public ModelAndView getMyTasks(Principal principal,String taskStatus) {
        ModelAndView model = new ModelAndView();
        if (principal == null) {
            model.setViewName("invalidSession");
            return model;
        }
        User user = userService.findUserByEmail(principal.getName());

        List<Task> tasks = new ArrayList<>();
        if(taskStatus.isEmpty()) {
            tasks = findTasksByUserAsc(user);
        }
        else{
            tasks = findTasksByUserAndTaskStatus(user,taskStatus);
        }
        model.addObject("tasks", tasks);
        model.setViewName("viewMyTasks");
        return model;
    }


    public ModelAndView updateTask(long taskId, Principal principal) {
        ModelAndView model = new ModelAndView();
        if (principal == null) {
            model.setViewName("invalidSession");
            return model;
        }
        Task task = taskRepository.findTaskById(taskId);
        TaskDto taskDto = new TaskDto();
        taskDto.setTaskName(task.getTaskName());
        taskDto.setTaskDescription(task.getTaskDescription());
        taskDto.setTaskTime(task.getTaskTime());
        taskDto.setUserName(task.getUser().getName());
        model.addObject("leadName", task.getLead().getName());
        model.addObject("taskId", task.getId());
        model.addObject("taskDto", taskDto);
        model.addObject("taskSaved", false);
        model.setViewName("editTask");
        return model;
    }

    public ModelAndView saveTaskChanges(TaskDto taskDto, BindingResult errors, Long taskId, String leadName, Principal principal) {
        ModelAndView model = new ModelAndView();
        if (principal == null) {
            model.setViewName("invalidSession");
            return model;
        }
        if (errors.hasErrors()) {
            model.setViewName("editTask");
            model.addObject("leadName", leadName);
            model.addObject("taskId", taskId);
            return model;
        }
        System.out.println("taskId is" + taskId);
        System.out.println(taskDto.getTaskName());
        System.out.println(taskDto.getUserName());
        System.out.println("Lead name is " + leadName);
        Task task = taskRepository.findTaskById(taskId);
        Lead lead = leadService.findLeadByName(leadName);
        task.setLead(lead);
        task.setUser(userService.findUserByName(taskDto.getUserName()));
        task.setTaskTime(taskDto.getTaskTime());
        task.setTaskName(taskDto.getTaskName());
        task.setTaskDescription(taskDto.getTaskDescription());
        taskRepository.save(task);
        model.addObject("taskSaved", true);
        model.addObject("leadName", leadName);
        model.setViewName("editTask");
        return model;
    }

    public ModelAndView deleteTask(Long taskId, Principal principal) {
        System.out.println("DELETING TASK ID " + taskId);
        Task task = taskRepository.findTaskById(taskId);
        taskRepository.delete(task);
        return getMyTasks(principal,"");
    }

    public ModelAndView viewOtherTasks(Principal principal) {
        ModelAndView model = new ModelAndView();
        if (principal == null) {
            model.setViewName("invalidSession");
            return model;
        }
        List<User> allUsers = getUsersByEmail(principal.getName());
        List<User> users = new ArrayList<>();
        for (User user : allUsers) {
            if (user.getRole().getName().equals("ROLE_USER")) {
                users.add(user);
            }
        }
        List<Task> tasks = new ArrayList<>();
        for (User user : users) {
            List<Task> t = findTasksByUserAsc(user);
            for (Task task : t) {
                tasks.add(task);
            }
        }
        model.addObject("tasks", tasks);
        model.setViewName("viewOthersTasks");
        return model;
    }


    public ModelAndView deleteOtherTask(Long taskId, Principal principal) {
        System.out.println("DELETING TASK ID " + taskId);
        Task task = taskRepository.findTaskById(taskId);
        taskRepository.delete(task);
        return viewOtherTasks(principal);
    }

    public List<Task> searchTaskByNameAndUser(User user, String taskName) {
        return taskRepository.findTaskByUserAndTaskNameLikeIgnoreCase(user, taskName);
    }

    public List<Task> getTotalTaskByUser(User user) {
        return taskRepository.findAllByUser(user);
    }


    public ModelAndView changeStatus(Long taskId,String status,Principal principal) {
        System.out.println("task id is "+taskId+"status is "+status);
        Task task = taskRepository.findTaskById(taskId);
        System.out.println(task.getTaskName());
        task.setTaskStatus(status);
        taskRepository.save(task);
        return getMyTasks(principal,"");
    }

    public List<Task> getTaskByLead(int id){
        return taskRepository.findByLead_Id(id);
    }

}
