package com.sales.crm.service;

import com.sales.crm.configuration.UserPrincipleForUser;
import com.sales.crm.model.*;
import com.sales.crm.repository.MessageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class AuthenticationService implements UserDetailsService {

    Logger logger = LoggerFactory.getLogger(AuthenticationService.class);
    @Autowired
    private UserService userService;
    @Autowired
    private EmailService emailService;
    @Autowired
    private LeadService leadService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private DealService dealService;
    @Autowired
    private MessageRepository messageRepository;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.getByEmailIgnoreCase(email);
        if (user == null) {
            logger.info("the user with mail " + email + " doesn't exit ");
            throw new UsernameNotFoundException("not found");
        }
        if (!user.isEnabled()) {
            throw new UsernameNotFoundException("Not enabled");
        }
        return new UserPrincipleForUser(user);
    }

    public String redirection(Authentication authentication, Model model) {
        User user = userService.getByEmailIgnoreCase(authentication.getName());
        if ((user.getRole().getName()).equals("ROLE_USER")) {
            return "redirect:/user";
        }
        return "redirect:/admin";
    }

    public String userDashboard(Authentication authentication, Model model) {
        User user = userService.getByEmailIgnoreCase(authentication.getName());
        List<Lead> leads = leadService.getByUser_IdOrderByPointsDesc(user.getId());
        List<Message> messages = messageService.getByUser_Id(user.getId());
        List<Task> tasks = taskService.findTasksByUserDesc(user);
        List<Deal> deals = dealService.getDealsByUser(user.getId());
        List<Deal> successfulDealList = dealService.getByLead_User_IdAndStatusTrue(user.getId());
        List<Deal> unSuccessfulDealWorthList = dealService.getByLead_User_IdAndStatusFalse(user.getId());
        int totalDeals = deals.size();
        int successfulDeals = successfulDealList.size();
        int unsuccessfulDeals = unSuccessfulDealWorthList.size();
        double successfulDealWorth = 0.0;
        for (Deal deal : successfulDealList) {
            successfulDealWorth += deal.getWorth();
        }
        double unSuccessfulDealWorth = 0.0;
        for (Deal deal : unSuccessfulDealWorthList) {
            unSuccessfulDealWorth += deal.getWorth();
        }
        int leadsConverted = 0;
        HashMap<Lead, Integer> leadHashMap = new HashMap<Lead, Integer>();
        for (Deal deal : successfulDealList) {
            if (!leadHashMap.containsKey(deal.getLead())) {
                leadHashMap.put(deal.getLead(), 1);
                leadsConverted++;
            }
        }
        int totalLeads = leads.size();
        int leadsPending = totalLeads - leadsConverted;
        int userTasks = taskService.getTotalTaskByUser(user).size();
        int userEmails = emailService.getMailSentByUser(user.getEmail()).size();
        int userMessages = messageService.getByUser_Id(user.getId()).size();
        model.addAttribute("totalTasks", userTasks);
        model.addAttribute("totalEmails", userEmails);
        model.addAttribute("totalMessages", userMessages);
        model.addAttribute("totalLeads", totalLeads);
        model.addAttribute("leadsConverted", leadsConverted);
        model.addAttribute("leadsPending", leadsPending);
        model.addAttribute("totalDeals", totalDeals);
        model.addAttribute("successfulDeals", successfulDeals);
        model.addAttribute("unSuccessfulDeals", unsuccessfulDeals);
        model.addAttribute("successfulDealWorth", successfulDealWorth);
        model.addAttribute("unSuccessfulDealWorth", unSuccessfulDealWorth);
        model.addAttribute("totalDealWorth", unSuccessfulDealWorth + successfulDealWorth);
        model.addAttribute("totalLeads", leads.size());
        model.addAttribute("messages", messages);
        model.addAttribute("leads", leads);
        model.addAttribute("user", user);
        model.addAttribute("tasks", tasks);
        model.addAttribute("deals", deals);
        return "userDashboard";

    }


    public String adminDashBoard(Authentication authentication, Model model, String sortBy, String
            selectUser) {
        User user = userService.findUserByEmail(authentication.getName());
        List<User> users = userService.findUserByCompany(user.getCompany());
        if (selectUser.isEmpty()) {

            List<Task> tasks = new ArrayList<>();
            for (User u : users) {
                if (sortBy.contains("Oldest first")) {
                    List<Task> t = taskService.findTasksByUserAsc(u);
                    for (Task task : t) {
                        tasks.add(task);

                    }
                } else {
                    List<Task> t = taskService.findTasksByUserDesc(u);
                    for (Task task : t) {
                        tasks.add(task);

                    }
                }
            }
            model.addAttribute("tasks", tasks);
        } else {
            User selectedUser = userService.findUserByName(selectUser);
            System.out.println("Select user is " + selectedUser.getName());
            List<Task> selectedUserTask = new ArrayList<>();
            if (sortBy.contains("Oldest first")) {
                selectedUserTask = taskService.findTasksByUserAsc(selectedUser);
            } else {
                selectedUserTask = taskService.findTasksByUserDesc(selectedUser);
            }
            model.addAttribute("tasks", selectedUserTask);
        }
        model.addAttribute("selectUsers", users);

        List<User> members = new ArrayList<>();
        List<Lead> teamLeads = new ArrayList<>();
        for(User mmb: users) {

            members.add(mmb);
        }

        for(User mmb: members) {
            List<Lead> userLeads = leadService.findLeadsByUser(mmb);
            for(Lead lead:userLeads) {
                teamLeads.add(lead);
            }
        }
        model.addAttribute("members",members);
        model.addAttribute("teamLeads",teamLeads);

        List<Deal> teamDeals = new ArrayList<>();
        for(User mmb:members) {
            List<Deal> d = dealService.getDealsByUser(mmb.getId());
            for(Deal deal:d) {
                teamDeals.add(deal);
            }
        }
        model.addAttribute("teamDeals",teamDeals);


        List<Lead> leads = leadService.getByUser_IdOrderByPointsDesc(user.getId());
        List<Message> messages = messageService.getByUser_Id(user.getId());
        List<Task> tasks = taskService.findTasksByUserDesc(user);
        List<Deal> deals = dealService.getDealsByUser(user.getId());
        List<Deal> successfulDealList = dealService.getByLead_User_IdAndStatusTrue(user.getId());
        List<Deal> unSuccessfulDealWorthList = dealService.getByLead_User_IdAndStatusFalse(user.getId());
        int totalDeals = deals.size();
        int successfulDeals = successfulDealList.size();
        int unsuccessfulDeals = unSuccessfulDealWorthList.size();
        double successfulDealWorth = 0.0;
        for (Deal deal : successfulDealList) {
            successfulDealWorth += deal.getWorth();
        }
        double unSuccessfulDealWorth = 0.0;
        for (Deal deal : unSuccessfulDealWorthList) {
            unSuccessfulDealWorth += deal.getWorth();
        }
        int leadsConverted = 0;
        HashMap<Lead, Integer> leadHashMap = new HashMap<Lead, Integer>();
        for (Deal deal : successfulDealList) {
            if (!leadHashMap.containsKey(deal.getLead())) {
                leadHashMap.put(deal.getLead(), 1);
                leadsConverted++;
            }
        }
        int totalLeads = leads.size();
        int leadsPending = totalLeads - leadsConverted;
        int userTasks = taskService.getTotalTaskByUser(user).size();
        int userEmails = emailService.getMailSentByUser(user.getEmail()).size();
        int userMessages = messageService.getByUser_Id(user.getId()).size();
        model.addAttribute("totalTasks", userTasks);
        model.addAttribute("totalEmails", userEmails);
        model.addAttribute("totalMessages", userMessages);
        model.addAttribute("totalLeads", totalLeads);
        model.addAttribute("leadsConverted", leadsConverted);
        model.addAttribute("leadsPending", leadsPending);
        model.addAttribute("totalDeals", totalDeals);
        model.addAttribute("successfulDeals", successfulDeals);
        model.addAttribute("unSuccessfulDeals", unsuccessfulDeals);
        model.addAttribute("successfulDealWorth", successfulDealWorth);
        model.addAttribute("unSuccessfulDealWorth", unSuccessfulDealWorth);
        model.addAttribute("totalDealWorth", unSuccessfulDealWorth + successfulDealWorth);
        model.addAttribute("totalLeads", leads.size());
        model.addAttribute("messages", messages);
        model.addAttribute("leads", leads);
        model.addAttribute("user", user);
        model.addAttribute("tasks", tasks);
        model.addAttribute("deals", deals);


        return "adminPage";

    }

    public String userProfile(Long userId, Model model) {
        User user = userService.getByUserId(userId);
        List<Lead> leads = leadService.getByUser_IdOrderByPointsDesc(user.getId());
        List<Message> messages = messageService.getByUser_Id(user.getId());
        List<Task> tasks = taskService.findTasksByUserDesc(user);
        List<Deal> deals = dealService.getDealsByUser(user.getId());
        List<Deal> successfulDealList = dealService.getByLead_User_IdAndStatusTrue(user.getId());
        List<Deal> unSuccessfulDealWorthList = dealService.getByLead_User_IdAndStatusFalse(user.getId());
        int totalDeals = deals.size();
        int successfulDeals = successfulDealList.size();
        int unsuccessfulDeals = unSuccessfulDealWorthList.size();
        double successfulDealWorth = 0.0;
        for (Deal deal : successfulDealList) {
            successfulDealWorth += deal.getWorth();
        }
        double unSuccessfulDealWorth = 0.0;
        for (Deal deal : unSuccessfulDealWorthList) {
            unSuccessfulDealWorth += deal.getWorth();
        }
        int leadsConverted = 0;
        HashMap<Lead, Integer> leadHashMap = new HashMap<Lead, Integer>();
        for (Deal deal : successfulDealList) {
            if (!leadHashMap.containsKey(deal.getLead())) {
                leadHashMap.put(deal.getLead(), 1);
                leadsConverted++;
            }
        }
        int totalLeads = leads.size();
        int leadsPending = totalLeads - leadsConverted;
        int userTasks = taskService.getTotalTaskByUser(user).size();
        int userEmails = emailService.getMailSentByUser(user.getEmail()).size();
        int userMessages = messageService.getByUser_Id(user.getId()).size();
        model.addAttribute("totalTasks", userTasks);
        model.addAttribute("totalEmails", userEmails);
        model.addAttribute("totalMessages", userMessages);
        model.addAttribute("totalLeads", totalLeads);
        model.addAttribute("leadsConverted", leadsConverted);
        model.addAttribute("leadsPending", leadsPending);
        model.addAttribute("totalDeals", totalDeals);
        model.addAttribute("successfulDeals", successfulDeals);
        model.addAttribute("unSuccessfulDeals", unsuccessfulDeals);
        model.addAttribute("successfulDealWorth", successfulDealWorth);
        model.addAttribute("unSuccessfulDealWorth", unSuccessfulDealWorth);
        model.addAttribute("totalDealWorth", unSuccessfulDealWorth + successfulDealWorth);
        model.addAttribute("totalLeads", leads.size());
        model.addAttribute("messages", messages);
        model.addAttribute("leads", leads);
        model.addAttribute("user", user);
        model.addAttribute("tasks", tasks);
        model.addAttribute("deals", deals);
        return "userDashboard";

    }
}


