package com.sales.crm.service;

import com.google.common.io.Files;
import com.sales.crm.dto.EmailDto;
import com.sales.crm.model.CustomEmail;
import com.sales.crm.model.User;
import com.sales.crm.repository.EmailRepository;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sales.crm.util.Constant.*;

@Service
public class EmailService {

    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private SendGrid sendGrid;
    @Autowired
    private EmailRepository emailRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private LeadService leadService;

    public ModelAndView getSendEmailPage(int leadId, Authentication authentication) {
        logger.debug("rendering getSendEmailPage");
        ModelAndView modelAndView = new ModelAndView();
        if (leadService.getByLeadId(leadId) == null) {
            modelAndView.setViewName("bad-request");
            logger.error("lead does not exist with this leadId {}", leadId);
            return modelAndView;
        }
        EmailDto emailDto = new EmailDto();
        emailDto.setReceiver(leadService.getByLeadId(leadId).getEmail());
        modelAndView.addObject("user", userService.findUserByMail(authentication.getName()));
        modelAndView.addObject("emailDto", emailDto);
        modelAndView.setViewName("send-email");
        return modelAndView;
    }

    public ModelAndView sendEmail(int leadId, EmailDto emailDto, BindingResult bindingResult,
                                  Authentication authentication, String sentAt,
                                  MultipartFile multipartFile) {
        logger.debug("rendering sendEmail with email {}", emailDto);
        ModelAndView modelAndView = new ModelAndView();
        if (leadService.getByLeadId(leadId) == null) {
            modelAndView.setViewName("bad-request");
            logger.error("lead does not exist with this leadId {}", leadId);
            modelAndView.addObject("user", userService.findUserByMail(authentication.getName()));
            return modelAndView;
        }
        modelAndView.setViewName("send-email");
        if (bindingResult.hasErrors()) {
            logger.error("errors occurred in the emailDto details {}", emailDto);
            modelAndView.addObject("successMessage", "invalid email id");
        } else {
            CustomEmail email = new CustomEmail();
            String[] ccArray = emailDto.getCc().split(SPLIT_REGEX);
            String[] bccArray = emailDto.getBcc().split(SPLIT_REGEX);
            for (String s : ccArray) {
                if (validateEmail(s) && !ccArray[0].isEmpty()) {
                    logger.error("invalid details in cc in {}", emailDto);
                    modelAndView.addObject("successMessage",
                            "error in cc field");
                    modelAndView.addObject("user", userService.findUserByMail(authentication.getName()));
                    return modelAndView;
                }
            }
            for (String s : bccArray) {
                if (validateEmail(s) && !bccArray[0].isEmpty()) {
                    logger.error("invalid details in bcc in {}", emailDto);
                    modelAndView.addObject("successMessage",
                            "error in bcc field");
                    modelAndView.addObject("user", userService.findUserByMail(authentication.getName()));
                    return modelAndView;
                }
            }

            try {
                Email from = new Email("sandysingh1918@gmail.com", "SALESCRM");
                String subject = emailDto.getSubject();
                Email to = new Email(emailDto.getReceiver());
                Content content = new Content("text/plain", emailDto.getMessage());
                Mail mail = new Mail(from, subject, to, content);
                mail.setReplyTo(new Email(
                        MAIL_PARSER_EMAIL_ADDRESS));
                mail.personalization.get(0).addSubstitution("username", "Some blog user");
                if (!multipartFile.isEmpty()) {
                    Attachments attachment = new Attachments();
                    File convertedFile = new File(Objects.requireNonNull(multipartFile.getOriginalFilename()));
                    convertedFile.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(convertedFile);
                    fileOutputStream.write(multipartFile.getBytes());
                    fileOutputStream.close();
                    attachment.setContent(Base64.getEncoder().encodeToString(
                            Files.toByteArray(convertedFile)));
                    attachment.setFilename(multipartFile.getName());
                    attachment.setType(multipartFile.getContentType());
                    mail.addAttachments(attachment);
                    email.setFile(convertedFile);
                }
                if (!sentAt.isEmpty()) {
                    Date date = null;
                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
                    try {
                        date = (Date) formatter.parse(sentAt);
                    } catch (Exception exception) {
                        logger.error(exception.getMessage());
                        exception.printStackTrace();
                        modelAndView.addObject("successMessage",
                                "invalid time");
                        modelAndView.addObject("user", userService.findUserByMail(authentication.getName()));
                        return modelAndView;
                    }
                    mail.setSendAt(date.getTime() / 1000L);
                }
                Request request = new Request();
                Response response = null;
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(mail.build());
                response = sendGrid.api(request);
                if (response.getStatusCode() >= 400) {
                    logger.error("sendgrid response code: " + response.getStatusCode());
                    logger.error("sendgrid response code body: " + response.getBody());
                    logger.error("sendgrid response code headers: " + response.getHeaders());
                    modelAndView.addObject("successMessage", "email could not" +
                            "be sent");
                    modelAndView.addObject("user", userService.findUserByMail(authentication.getName()));
                    return modelAndView;
                } else {
                    logger.debug("sendgrid response code: " + response.getStatusCode());
                    logger.debug("sendgrid response code body: " + response.getBody());
                    logger.debug("sendgrid response code headers: " + response.getHeaders());
                    email.setReceiver(emailDto.getReceiver());
                    email.setSubject(emailDto.getSubject());
                    email.setMessage(emailDto.getMessage());
                    email.setSender(authentication.getName());
                    email.setCc(emailDto.getCc());
                    email.setBcc(emailDto.getBcc());
                    emailRepository.save(email);
                    EmailDto newEmailDto = new EmailDto();
                    newEmailDto.setReceiver(leadService.getByLeadId(leadId).getEmail());
                    modelAndView.addObject("emailDto", newEmailDto);
                    modelAndView.addObject("successMessage", "mail sent successfully");
                    logger.debug("email has been sent successfully {}", emailDto);
                }
            } catch (Exception exception) {
                logger.error(exception.getMessage());
                exception.printStackTrace();
                modelAndView.addObject("user", userService.findUserByMail(authentication.getName()));
                modelAndView.addObject("successMessage", exception.getMessage());
                return modelAndView;
            }
        }
        modelAndView.addObject("user", userService.findUserByMail(authentication.getName()));
        return modelAndView;
    }

    public ModelAndView getEmails(Authentication authentication) {
        logger.debug("rendering getEmails");
        ModelAndView modelAndView = new ModelAndView();
        EmailDto[] emailList = null;
        try {
            emailList = fetchEmailsFromMailParserApi(new RestTemplate());
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            modelAndView.addObject("successMessage", "error occurred in fetching mails");
            modelAndView.setViewName("bad-request");
            return modelAndView;
        }
        User user = userService.getByEmailIgnoreCase(authentication.getName());
        modelAndView.addObject("emails", emailList);
        modelAndView.setViewName("view-emails");
        modelAndView.addObject("user", user);
        logger.debug("mails fetched from mail parser");
        return modelAndView;
    }

    public ModelAndView getEmailByEmailId(String emailId, Authentication authentication) {
        logger.debug("rendering getEmailById");
        ModelAndView modelAndView = new ModelAndView();
        EmailDto[] emailList = null;
        try {
            emailList = fetchEmailsFromMailParserApi(new RestTemplate());
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            modelAndView.addObject("successMessage", "error occurred in fetching mails");
            modelAndView.setViewName("bad-request");
            return modelAndView;
        }
        boolean emailExist = false;
        for (EmailDto emailDto : emailList) {
            if (emailDto.getId().equals(emailId)) {
                modelAndView.addObject("emailDto", emailDto);
                emailExist = true;
                System.out.println(emailDto.toString());
            }
        }
        if (emailExist) {
            User user = userService.getByEmailIgnoreCase(authentication.getName());
            modelAndView.setViewName("view-one-email");
            modelAndView.addObject("user", user);
            logger.debug("mails fetched from mail parser with id {}", emailId);
        } else {
            logger.error("email does not exist with emailId {}", emailId);
            modelAndView.addObject("successMessage", "email with id" +
                    emailId + " does not exist");
            modelAndView.setViewName("bad-request");
        }
        return modelAndView;
    }

    public List<CustomEmail> getMailSentByUser(String email) {
        return emailRepository.findAllBySender(email);
    }

    public EmailDto[] fetchEmailsFromMailParserApi(RestTemplate restTemplate) throws Exception {
        ResponseEntity<EmailDto[]> response = restTemplate.getForEntity(
                MAIL_PARSER_API_LINK,
                EmailDto[].class);
        return response.getBody();
    }

    public boolean validateEmail(final String email) {
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        return !matcher.matches();
    }
    public List<CustomEmail> getMailByReciever(String email){return emailRepository.findByReceiver(email);}

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
