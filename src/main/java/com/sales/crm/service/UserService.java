package com.sales.crm.service;

import com.sales.crm.dto.PasswordDto;
import com.sales.crm.dto.UserDto;
import com.sales.crm.exception.CompanyAlreadyExistException;
import com.sales.crm.exception.UserAlreadyExistException;
import com.sales.crm.model.Company;
import com.sales.crm.model.PasswordResetToken;
import com.sales.crm.model.User;
import com.sales.crm.model.UserVerificationToken;
import com.sales.crm.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleService roleService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private PasswordResetTokenService passwordResetTokenService;
    @Autowired
    private UserVerificationTokenService userVerificationTokenService;

    public User findUserByMail(String mail) {
        return userRepository.findByEmailIgnoreCase(mail);
    }


    public User registerNewUser(final UserDto userDto) {
        if (emailExists(userDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email address: "
                    + userDto.getEmail());
        }
        if (companyExists(userDto.getCompanyDto().getName())) {
            throw new CompanyAlreadyExistException(userDto.getCompanyDto().getName());
        }

        final User user = new User();

        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setMobile(userDto.getMobile());
        Company company = new Company();
        company.setName(userDto.getCompanyDto().getName());
        companyService.save(company);
        user.setCompany(company);
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setRole(roleService.getByName("ROLE_ADMIN"));
        return userRepository.save(user);
    }

    public User addUser(UserDto userDto, Principal principal) {
        System.out.println("IN MEMBER REG METHOD");
        if (emailExists(userDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email adress: "
                    + userDto.getEmail());
        }
        final User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setMobile(userDto.getMobile());
        User admin = userRepository.findByEmail(principal.getName());
        Company company = admin.getCompany();
        user.setCompany(company);
        user.setPassword(passwordEncoder.encode("mohak123"));
        user.setRole(roleService.getByName("ROLE_USER"));
        return userRepository.save(user);
    }

    public User getUser(final String token) {
        final UserVerificationToken userVerificationToken =
                userVerificationTokenService.getByToken(token);
        if (token != null) {
            return userVerificationToken.getUser();
        }
        return null;
    }


    public User confirmUser(PasswordDto dto) {
        User user = userRepository.findByEmail(dto.getEmail());
        logger.info("user is " + user.getEmail());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setEnabled(true);
        return userRepository.save(user);
    }

    public void saveRegisteredUser(final User user) {
        userRepository.save(user);
    }

    public boolean emailExists(final String email) {
        return userRepository.findByEmail(email) != null;
    }

    private boolean companyExists(final String name) {
        return companyService.getByName(name) != null;
    }

    public List<User> findUserByCompany(Company company) {
        return userRepository.findUsersByCompanyAndEnabledTrue(company);
    }

    public User findUserByName(String username) {
        return userRepository.findUserByNameAndEnabledTrue(username);
    }

    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    public void createVerificationTokenForUser(final User user, final String token) {
        final UserVerificationToken userVerificationToken = new UserVerificationToken(token, user);
        userVerificationTokenService.save(userVerificationToken);
    }

    public UserVerificationToken generateNewUserVerificationToken(final String existingVerificationToken) {
        UserVerificationToken userVerificationToken = userVerificationTokenService.
                getByToken(existingVerificationToken);
        userVerificationToken.updateToken(UUID.randomUUID()
                .toString());
        return userVerificationTokenService.save(userVerificationToken);
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmailIgnoreCase(email);
    }

    public void createPasswordResetTokenForUser(final User user, final String token) {
        final PasswordResetToken passwordResetToken = new PasswordResetToken(token, user);
        passwordResetTokenService.save(passwordResetToken);
    }

    public void deletePasswordResetToken(final PasswordResetToken passwordResetToken) {
        passwordResetTokenService.delete(passwordResetToken);
    }

    public PasswordResetToken getPasswordResetToken(final String token) {
        return passwordResetTokenService.getByToken(token);
    }

    public void changeUserPassword(final User user, final String password) {
        user.setPassword(passwordEncoder.encode(password));
        userRepository.save(user);
    }

    public User getByEmailIgnoreCase(String email) {
        return userRepository.findByEmailIgnoreCase(email);
    }

    public User getByUserId(Long userId) {
        return userRepository.findById(userId).get();
    }

    public List<User> findUsersByCompany(String comapny) {
        return userRepository.findByCompany_Name(comapny);
    }

    public List<User> findByCompany(Company company) {
        return userRepository.findUsersByCompanyAndEnabledTrue(company);
    }
}
