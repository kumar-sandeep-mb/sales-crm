package com.sales.crm.service;

import com.sales.crm.dto.LeadDto;
import com.sales.crm.model.*;
import com.sales.crm.repository.LeadRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Service
public class LeadService {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private DealService dealService;
    @Autowired
    private LeadRepository leadRepository;
    @Autowired
    private TaskService taskService;
    @Autowired
    private EmailService emailService;

    public ModelAndView addLeadPreFunction(Authentication authentication) {
        logger.info("renderring the add lead prefunction to get the page");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("leadDto", new LeadDto());
        modelAndView.addObject("message", false);
        modelAndView.addObject("user",userService.getByEmailIgnoreCase(authentication.getName()));
        modelAndView.setViewName("addLead");
        return modelAndView;
    }

    public ModelAndView addLead(LeadDto leadDto, BindingResult bindingResult, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user",userService.getByEmailIgnoreCase(authentication.getName()));
        logger.info("renderring the save lead function to save lead");
        if (bindingResult.hasErrors()) {
            logger.warn("validation Failure in save lead");
            modelAndView.addObject("message", false);
            modelAndView.setViewName("addLead");
            return modelAndView;
        }
        User user = userService.findUserByMail(authentication.getName());
        String company = user.getCompany().getName();
        List<Lead> companyLeads = leadRepository.findByUser_Company_Name(company);
        for (Lead lead : companyLeads) {
            if (lead.getName().equals(leadDto.getName())) {
                logger.warn("Name already exits with the teamamtes");
                bindingResult.rejectValue("name", "error.user", "name alredy exits choose diffeent name");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.addObject("message", false);
                modelAndView.setViewName("addLead");
                return modelAndView;
            }

        }
        for (Lead lead : companyLeads) {
            if (lead.getEmail().equals(leadDto.getEmail())) {
                logger.warn("Email already exits with the teamamtes");
                bindingResult.rejectValue("email", "error.user", "Hey User this lead mail has" +
                        " already with your teammate");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.addObject("message", false);
                modelAndView.setViewName("addLead");
                return modelAndView;
            }

        }

        final Lead lead = new Lead();
        lead.setUser(user);
        lead.setEmail(leadDto.getEmail());
        lead.setMobile(leadDto.getMobile());
        lead.setName(leadDto.getName());
        lead.setPosition(leadDto.getPosition());
        lead.setPoints(getpoints(leadDto));
        logger.info("lead saves to the database");
        leadRepository.save(lead);
        modelAndView.setViewName("addLead");
        modelAndView.addObject("leadDto", new LeadDto());
        modelAndView.addObject("message", true);
        return modelAndView;
    }

    public ModelAndView assignLeadPreFunction(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView();
        logger.info("renderring the assign lead prefunction to get the page");
        User user = userService.findUserByMail(authentication.getName());
        List<User> users = userService.findUserByCompany(user.getCompany());
        modelAndView.addObject("users", users);
        modelAndView.addObject("leadDto", new LeadDto());
        modelAndView.setViewName("assignLead");
        modelAndView.addObject("message", false);
        return modelAndView;
    }

    public ModelAndView saveAssignedLead(LeadDto leadDto, BindingResult bindingResult, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView();
        logger.info("renderring the save lead function to save the lead");
        User user = userService.findUserByMail(authentication.getName());
        List<User> users = userService.findUserByCompany(user.getCompany());
        modelAndView.addObject("users",users);
        if(bindingResult.hasErrors()){
            logger.warn("validation errror in assigne lead function");
            modelAndView.setViewName("assignLead");
            return modelAndView;
        }
        String company = user.getCompany().getName();
        List<Lead> companyLeads = leadRepository.findByUser_Company_Name(company);
        for (Lead lead : companyLeads) {
            if (lead.getName().equals(leadDto.getName())) {
                logger.warn("Name already exits with the teamamtes");
                bindingResult.rejectValue("name", "error.user", "Name already exits");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.setViewName("assignLead");
                return modelAndView;
            }

        }
        for (Lead lead : companyLeads) {
            if (lead.getEmail().equals(leadDto.getEmail())) {
                logger.warn("Email already exits with the teamamtes");
                bindingResult.rejectValue("email", "error.user", "Hey User this lead mail has" +
                        " already with your teammate");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.setViewName("assignLead");
                return modelAndView;
            }
        }

        Lead lead = new Lead();
        lead.setName(leadDto.getName());
        lead.setEmail(leadDto.getEmail());
        lead.setMobile(leadDto.getMobile());
        lead.setEnabled(true);
        lead.setPosition(leadDto.getPosition());
        lead.setUser(leadDto.getUser());
        lead.setPoints(getpoints(leadDto));
        leadRepository.save(lead);
        logger.info("assigend lead succefully saved to lead data base" );
        modelAndView.addObject("leadDto",new LeadDto());
        modelAndView.addObject("message",true);
        modelAndView.setViewName("assignLead");
        return modelAndView;
    }

    public ModelAndView editLeadPreFunction(int leadId) {
        logger.info("renderring the edit lead prefunction to get the page");
        ModelAndView modelAndView = new ModelAndView();
        Lead lead = leadRepository.findById(leadId);
        LeadDto leadDto = new LeadDto();
        leadDto.setEmail(lead.getEmail());
        leadDto.setMobile(lead.getMobile());
        leadDto.setName(lead.getName());
        leadDto.setPosition(lead.getPosition());
        modelAndView.addObject("leadDto", leadDto);
        modelAndView.addObject("message", "");
        modelAndView.setViewName("editLead");
        return modelAndView;
    }


    public ModelAndView editLead(LeadDto leadDto, BindingResult bindingResult,
                                 int leadId,Authentication authentication) {

        ModelAndView modelAndView = new ModelAndView();
        Lead lead = leadRepository.findById(leadId);
        logger.info("renderring the edit lead function to edit lead");
        if (bindingResult.hasErrors()) {
            logger.warn("Validation failure in edit lead function");
            modelAndView.addObject("message", "");
            modelAndView.setViewName("editLead");
            return modelAndView;
        }

        User user = userService.findUserByMail(authentication.getName());
        String company = user.getCompany().getName();
        List<Lead> companyleads = leadRepository.findByUser_Company_Name(company);
        for (Lead leadobj : companyleads) {
            if (leadobj.getName().equals(lead.getName())) {
                continue;
            }
            if (leadobj.getName().equals(leadDto.getName())) {
                logger.warn("Name already exit");
                bindingResult.rejectValue("name", "error.lead", "Name already exits");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.addObject("message", "");
                modelAndView.setViewName("editLead");
                return modelAndView;
            }
        }
        for (Lead leadobj : companyleads) {
            if (leadobj.getEmail().equals(lead.getEmail())) {
                continue;
            }
            if (leadobj.getEmail().equals(leadDto.getEmail())) {
                logger.warn("Email already with your teamates");
                bindingResult.rejectValue("email", "error.lead", "Hey User this lead Gmail " +
                        " is alredy with your team");
                modelAndView.addAllObjects(bindingResult.getModel());
                modelAndView.addObject("message", "");
                modelAndView.setViewName("editLead");
                return modelAndView;
            }
        }

        lead.setEmail(leadDto.getEmail());
        lead.setPosition(leadDto.getPosition());
        lead.setName(leadDto.getName());
        lead.setMobile(leadDto.getMobile());
        lead.setPosition(leadDto.getPosition());
        lead.setPoints(getpoints(leadDto));
        leadRepository.save(lead);
        logger.info("lead edited and saved to the database");
        modelAndView.addObject("message", " Lead Edited");
        modelAndView.setViewName("editLead");
        return modelAndView;

    }


    public ModelAndView leadAction(int leadId, Authentication authentication) {
        logger.info("renderring the lead Action to display the infrmation about the lead");
        ModelAndView modelAndView = new ModelAndView();
        User user = userService.findUserByMail(authentication.getName());
        Lead lead = leadRepository.findById(leadId);
        List<Deal> deals = dealService.findLeadByLeadId(leadId);
        List<Message> messages = messageService.findMessageByLeadId(leadId);
        List<Deal> closedDeal = dealService.findDealStatusTrue(leadId);
        List<Deal> openDeal = dealService.findDealStatusFalse(leadId);
        List<Task> tasks = taskService.getTaskByLead(leadId);
        Double closedDealWorth = 0.0;
        for (Deal deal : closedDeal) {
            closedDealWorth = closedDealWorth + deal.getWorth();
        }
        Double openDealWorth = 0.0;
        for (Deal deal : openDeal) {
            openDealWorth = openDealWorth + deal.getWorth();
        }
        List<CustomEmail> emails = emailService.getMailByReciever(lead.getEmail());
        modelAndView.addObject("closedDeal",closedDeal);
        modelAndView.addObject("openDeal",openDeal);
        modelAndView.addObject("totalDeal",(closedDeal.size())+(openDeal.size()));
        modelAndView.addObject("closedDealWorth",closedDealWorth);
        modelAndView.addObject("openDealWorth",openDealWorth);
        modelAndView.addObject("totalWorth",closedDealWorth+openDealWorth);
        modelAndView.addObject("user",user);
        modelAndView.addObject("lead",lead);
        modelAndView.addObject("messages",messages);
        modelAndView.addObject("deals",deals);
        modelAndView.addObject("emails",emails);
        modelAndView.addObject("leadId",leadId);
        modelAndView.addObject("tasks",tasks);
        modelAndView.setViewName("leaddetails");
        return modelAndView;
    }

    private int getpoints(LeadDto leadDto) {
        logger.info("renderring the getPoints to set the points to users");
        if(leadDto.getPosition().equals("Ceo")){
            return 15;
        }else if(leadDto.getPosition().equals("Manager")){
            return 10;
        }else  if(leadDto.getPosition().equals("Developer")){
            return 5;
        }else {
            return 2;
        }
    }

    public Lead findLead(int leadId){
        return leadRepository.findById(leadId);
    }

    public void saveLead(Lead lead) {
        leadRepository.save(lead);
    }

    public List<Lead> getByUser_IdOrderByPointsDesc(Long leadId) {
        return leadRepository.findByUser_IdOrderByPointsDesc(leadId);
    }

    public Lead getByLeadId(int leadId) {
        return leadRepository.findById(leadId);
    }

    public List<Lead> findLeadsByUser(User user) {
        return leadRepository.findLeadsByUser(user);
    }

    public Lead findLeadByName(String leadName) {
        return leadRepository.findLeadByName(leadName);
    }


}
