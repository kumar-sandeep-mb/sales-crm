package com.sales.crm.service;

import com.sales.crm.model.User;
import com.sales.crm.model.UserVerificationToken;
import com.sales.crm.repository.UserVerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserVerificationTokenService {

    @Autowired
    UserVerificationTokenRepository userVerificationTokenRepository;

    public UserVerificationToken getByToken(String token) {
        return userVerificationTokenRepository.findByToken(token);
    }

    public UserVerificationToken save(UserVerificationToken userVerificationToken) {
        return userVerificationTokenRepository.save(userVerificationToken);
    }

    public UserVerificationToken getByUser(User user) {
        return userVerificationTokenRepository.findByUser(user);
    }
}
