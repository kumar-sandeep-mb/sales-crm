package com.sales.crm.validation;

import com.google.common.base.Joiner;
import org.passay.*;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

import static com.sales.crm.util.Constant.*;

public class PasswordConstraintValidator implements ConstraintValidator<ValidPassword, String> {

    @Override
    public void initialize(final ValidPassword arg0) {

    }

    @Override
    public boolean isValid(final String password, final ConstraintValidatorContext context) {
        final PasswordValidator validator = new PasswordValidator(Arrays.asList(
                new LengthRule(MINIMUM_PASSWORD_LENGTH, MAXIMUM_PASSWORD_LENGTH),
                new UppercaseCharacterRule(MINIMUM_UPPER_CASE_CHARACTER_IN_PASSWORD),
                new DigitCharacterRule(MINIMUM_DIGITS_IN_PASSWORD),
                new SpecialCharacterRule(MINIMUM_SPECIAL_CHARACTER_IN_PASSWORD),
                new NumericalSequenceRule(NUMERICAL_SEQUENCE_LENGTH, false),
                new AlphabeticalSequenceRule(ALPHABETICAL_SEQUENCE_LENGTH, false),
                new QwertySequenceRule(QWERTY_SEQUENCE_LENGTH, false),
                new WhitespaceRule()));
        final RuleResult result = validator.validate(new PasswordData(password));
        if (result.isValid()) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(Joiner.on(",").join(validator.getMessages(result)))
                .addConstraintViolation();
        return false;
    }

}
