package com.sales.crm.dto;

import com.sales.crm.validation.ValidPassword;

import javax.validation.constraints.NotBlank;

public class ResetPasswordDto {

    private String passwordResetToken;

    @ValidPassword
    private String password;

    @NotBlank
    private String matchingPassword;

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }
}
