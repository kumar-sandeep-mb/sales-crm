package com.sales.crm.dto;

import com.opencsv.bean.CsvBindByName;
import com.sales.crm.validation.ValidEmail;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class UserCsvDto {

    @CsvBindByName
    @NotBlank
    private String name;

    @CsvBindByName
    @ValidEmail
    private String email;

    @CsvBindByName
    @Pattern(regexp = "(\6|7|8|9)[0-9]{9}")
    private String mobile;

    public UserCsvDto() {
    }

    public UserCsvDto(String name, String email, String mobile) {
        this.name = name;
        this.email = email;
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Override
    public String toString() {
        return "UserCsvDto{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
