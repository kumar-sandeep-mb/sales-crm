package com.sales.crm.dto;

import com.sales.crm.model.Deal;
import com.sales.crm.model.Lead;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;

public class DealsDto {

    @NotEmpty(message = "ENTER THE DEAL NAME")
    private String dealName;

    @DecimalMin(value = "10.0", message = "ENTER THE WORTH OF DEAL MORE THAN 10")
    private double worth;

    private String userName;

   private Lead lead;

    public Lead getLead() {
        return lead;
    }

    public void setLead(Lead lead) {
        this.lead = lead;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDealName() {
        return dealName;
    }

    public void setDealName(String dealName) {
        this.dealName = dealName;
    }

    public double getWorth() {
        return worth;
    }

    public void setWorth(double worth) {
        this.worth = worth;
    }


    @Override
    public String toString() {
        return "DealsDto{" +
                "dealName='" + dealName + '\'' +
                ", worth=" + worth +
                '}';
    }
}
