package com.sales.crm.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;


public class TaskDto {

    @NotBlank(message = "PLEASE ASSIGN NAME TO TASK.")
    @Size(min = 5, message = "TASK NAME SHOULD BE ATLEAST 5 CHARACTERS LONG.")
    private String taskName;

    @NotBlank(message = "PLEASE ASSIGN A DESCRIPTION TO TASK.")
    @Size(min = 10, message = "DESCRIPTION SHOULD BE ATLEAST 10 CHARACTERS LONG.")
    private String taskDescription;

    @NotNull(message = "PLEASE ENTER A VALID TIME FOR THIS TASK.")
    @Future(message = "PLEASE ENTER A VALID TIME FOR THIS TASK.")
    @DateTimeFormat(pattern = "dd/MM/yyyy h:mm a")
    private Date taskTime;

    private String userName;


    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Date getTaskTime() {
        return taskTime;
    }

    public void setTaskTime(Date taskTime) {
        this.taskTime = taskTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
