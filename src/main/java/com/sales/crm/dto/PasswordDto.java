package com.sales.crm.dto;

import com.sales.crm.validation.ValidPassword;

import javax.validation.constraints.NotBlank;

public class PasswordDto {

    private String email;

    @ValidPassword
    @NotBlank(message="PLEASE ENTER A VALID PASSWORD.")
    private String password;

    @NotBlank(message="PLEASE ENTER PASSWORD AGAIN.")
    private String matchingPassword;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }
}
