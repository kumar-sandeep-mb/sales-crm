package com.sales.crm.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CompanyDto {
    @Size(min = 1, max = 50)
    @NotNull(message = "Company name")
    private String name;

    public CompanyDto() {

    }

    public CompanyDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
