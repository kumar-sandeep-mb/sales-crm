package com.sales.crm.dto;

import com.sales.crm.validation.PasswordMatches;
import com.sales.crm.validation.ValidEmail;
import com.sales.crm.validation.ValidPassword;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@PasswordMatches
public class UserDto {

    Long id;

    @NotBlank(message = "NAME FIELD CANNOT BE BLANK.")
    private String name;

    @ValidEmail(message = "PLEASE ENTER A VALID EMAIL ADDRESS.")
    private String email;

    @Pattern(regexp = "(\6|7|8|9)[0-9]{9}", message = "PLEASE ENTER A VALID PHONE NUMBER.")
    private String mobile;

    private CompanyDto companyDto;

    @ValidPassword
    private String password;

    @NotBlank(message = "PASSWORD SHOULD MATCH.")
    private String matchingPassword;

    public UserDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public CompanyDto getCompanyDto() {
        return companyDto;
    }

    public void setCompanyDto(CompanyDto companyDto) {
        this.companyDto = companyDto;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(final String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("UserDto [name=").append(name).append(", password=").append(password)
                .append(", matchingPassword=").append(matchingPassword).append(", email=").append(email)
                .append(",mobile=").append(mobile).append(",company=").append(companyDto.getName());
        return builder.toString();
    }

}
