package com.sales.crm.dto;

import java.util.List;

public class IncorrectDataDto {

    public List<String> badUsers;

    public List<String> getBadUsers() {
        return badUsers;
    }

    public void setBadUsers(List<String> badUsers) {
        this.badUsers = badUsers;
    }
}
