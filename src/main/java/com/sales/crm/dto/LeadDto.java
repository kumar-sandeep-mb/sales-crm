package com.sales.crm.dto;


import com.sales.crm.model.User;
import com.sales.crm.validation.ValidEmail;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class LeadDto {

    @NotEmpty(message = "ENTER THE NAME")
    private String name;

    @ValidEmail
    private String email;

    @Pattern(regexp = "(\6|7|8|9)[0-9]{9}", message = "PLEASE PROVIDE VALID NUMBER WHICH START WITH 6,7,8,9 AND SIZE OF 10")
    private String mobile;


    @NotEmpty(message = "CHOSE ANY ONE ROLE")
    private String position;

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("UserDto [name=").append(name).append(", email =").append(email)
                .append(",mobile=").append(mobile).append(",Position=").append(position);
        return builder.toString();
    }
}


